package com.hokai.daily.service;

import java.util.List;
import com.hokai.daily.entity.BusDailyEntity;
import com.hokai.utils.Page;

public interface DailyService {

	    //新增日报
		int addDaily(BusDailyEntity bde);
		
		//删除日报(并非真正删除而是改变数据的状态)
		int deleteDailyByUserIdAndIdAndGroupId(BusDailyEntity bde);
		
		//编辑日报
		int updateDailyByUseridAndIdAndGroupId(BusDailyEntity bde);
		
		//查询展示日报(包括根据条件查询)
		List<BusDailyEntity> queryAllDaily(Page<BusDailyEntity> page);
}
