package com.hokai.daily.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hokai.daily.dao.DailyMapper;
import com.hokai.daily.entity.BusDailyEntity;
import com.hokai.utils.Page;
@Service
public class DailyServiceImpl implements DailyService {

	@Autowired
	DailyMapper dailyMapper;
	/**
	 * 新增日报
	 */
	@Override
	public int addDaily(BusDailyEntity bde){
		Map<String, Object> mapData = new HashMap<String, Object>();
		mapData.put("user_id", bde.getUser_id());
		mapData.put("daily_date", bde.getDaily_date());//--------------时间格式的转换可能存在问题
		mapData.put("group_id", bde.getGroup_id());
		mapData.put("status", bde.getStatus());
		mapData.put("delete_status", bde.getDelete_status());
		//先去查询今日日报是否已经提交了一次，若提交了一次，就不能再提交了，只能编辑或删除掉
		BusDailyEntity daily = dailyMapper.findDailyByUserIdAndDailyDateAndGroupId(mapData);
        	//如果当天还没有添加日报，则允许添加
        	if(daily == null){
        		int num = dailyMapper.addDaily(bde);
        		return num; //1 表时提交成功
        	}else{
        		return 2; // 2 代表异常，已经提交了不能在进行提交
        	}
	}

	/**
	 * 删除日报
	 */
	@Override
	public int deleteDailyByUserIdAndIdAndGroupId(BusDailyEntity bde) {
		Map<String, Object> mapData = new HashMap<String, Object>();
		mapData.put("user_id", bde.getUser_id());
		mapData.put("daily_date", bde.getDaily_date());
		mapData.put("group_id", bde.getGroup_id());
		mapData.put("status", bde.getStatus());
		mapData.put("delete_status", bde.getDelete_status());
		//先去查询某日日报是否存在，存在才能删除
		//BusDailyEntity daily = dailyMapper.findDailyByUserIdAndDailyDateAndGroupId(mapData);
		
		int flug = dailyMapper.deleteDailyByUserIdAndIdAndGroupId(bde);
        return flug;
	}

	/**
	 * 编辑日报
	 */
	@Override
	public int updateDailyByUseridAndIdAndGroupId(BusDailyEntity bde) {
		System.out.println(dailyMapper.updateDailyByUseridAndIdAndGroupId(bde));
       return dailyMapper.updateDailyByUseridAndIdAndGroupId(bde);
	}

	/**
	 * 查询展示日报信息
	 */
	@Override
	public List<BusDailyEntity> queryAllDaily(Page<BusDailyEntity> page) {
		return dailyMapper.queryAllDaily(page);
	}

}
