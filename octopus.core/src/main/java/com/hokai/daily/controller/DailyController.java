package com.hokai.daily.controller;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hokai.base.common.ResultMessage;
import com.hokai.daily.entity.BusDailyEntity;
import com.hokai.daily.service.DailyService;
import com.hokai.sysManage.bo.TUserEntity;
import com.hokai.sysManage.dao.TUserProjectMapper;
import com.hokai.utils.DateUtil;
import com.hokai.utils.Page;

@Controller
@RequestMapping("/daily")
public class DailyController {

	@Autowired
	DailyService dailyService;
	
	@Autowired
	TUserProjectMapper tUserProjectMapper;
	/**
	 * 新增日报
	 * @param request
	 * @param group_id
	 * @param daily_date
	 * @param completed_task
	 * @param uncompleted_task
	 * @param output_result
	 * @param today_plan
	 * @param need_help_content
	 * @return
	 */
	/*@ApiOperation(value="新增日报",notes="可以填写每天的日报")
	@ApiImplicitParams({
		@ApiImplicitParam(name="group_id",value="项目组id",required=true,paramType="query",dataType="String"),
		@ApiImplicitParam(name="daily_date",value="日报填写时间",required=true,paramType="query",dataType="String"),
		@ApiImplicitParam(name="completed_task",value="昨日完成任务",required=true,paramType="query",dataType="String"),
	}) ,produces="application/json",consumes="application/json"*/
	@RequestMapping(value = "/addDaily", method = RequestMethod.POST) // produces只接受json格式的数据
	@ResponseBody
	public ResultMessage addDaily(HttpServletRequest request,String group_id,String daily_date,String completed_task,
			String uncompleted_task,String output_result,String today_plan,String need_help_content){
		ResultMessage result = new ResultMessage();
		BusDailyEntity daily = new BusDailyEntity();
			
			//1. 项目组ID
			if(StringUtils.isNotBlank(group_id)){  //判断从前台传过的是否为空值
				Integer group_id_val = Integer.parseInt(group_id);  //从前台传过来的是String类型，而后台数据库需要的是int类型
				daily.setGroup_id(group_id_val);
			}
			//2. 日报提交时间
			if(StringUtils.isNotBlank(daily_date)){
				daily.setDaily_date(DateUtil.strToYYMMDDDate(daily_date));
				daily.setCreate_date(DateUtil.strToDate(daily_date));
			}
			//3. 昨日已完成任务
			if(StringUtils.isNotBlank(completed_task)){
				daily.setCompleted_task(completed_task);
			}
			//4. 未完成任务
			if(StringUtils.isNotBlank(uncompleted_task)){
				daily.setUncompleted_task(uncompleted_task);
			}
			//5. 输出成果物
			if(StringUtils.isNotBlank(output_result)){
				daily.setOutput_result(output_result);
			}
			//6. 今日计划
			if(StringUtils.isNotBlank(today_plan)){
				daily.setToday_plan(today_plan);
			}
			//7. 需要支持
			if(StringUtils.isNotBlank(need_help_content)){  //判断从前台传过的是否为空值
				daily.setNeed_help_content(need_help_content);
			}
			//8. 提交状态(1：已经提交   0：未提交)
			daily.setStatus(1);
			//9. 是否删除(1：未删除    0：已删除)
			daily.setDelete_status(1);
			
			
			daily.setUser_id(getUserIdBySecurity()); //------------获取user_id数据
			daily.setCreate_user_id(getUserIdBySecurity());
			int num = dailyService.addDaily(daily);
			if(num == 1){
				result.setMessage("日报提交成功");
				result.setResult("1");
				return result;
			}else{
				result.setMessage("日报已经提交,只可编辑/删除");
				result.setResult("2");
				return result;
			}
	}
	
	/**
	 * 删除日报
	 * @param request
	 * @param user_id
	 * @param id
	 * @param group_id
	 * @return
	 */
	@RequestMapping(value = "/deleteDaily", method = RequestMethod.POST)
	@ResponseBody
	public ResultMessage deleteDailyByUserIdAndIdAndGroupId(HttpServletRequest request,String id){ 
		ResultMessage result = new ResultMessage();
		BusDailyEntity daily = new BusDailyEntity();
		//1. 项目组ID
		/*if(StringUtils.isNotBlank(group_id)){  //判断从前台传过的是否为空值
			Integer group_id_val = Integer.parseInt(group_id);  //从前台传过来的是String类型，而后台数据库需要的是int类型
			daily.setGroup_id(group_id_val);
		}*/
		//2. 获取当前数据在库中的id
		System.out.println("id-------:"+id);
		if(StringUtils.isNotBlank(id)){  
			Integer id_val = Integer.parseInt(id);
			daily.setId(id_val);
		}
		//3. 提交状态(1：已经提交   0：未提交)
		daily.setStatus(0);
		//4. 是否删除状态(1：未删除    0：已删除)
		daily.setDelete_status(0);
		//5. 删除时间
		daily.setDelete_date(new Date());
		//6. 删除人id  
		daily.setDelete_user_id(getUserIdBySecurity());
		
		
		int flug = dailyService.deleteDailyByUserIdAndIdAndGroupId(daily);
		if(1 == flug){
			result.setMessage("日报删除成功");
			result.setResult("1");
			return result;
		}else{
			result.setMessage("日报删除失败");
			result.setResult("2");
			return result;
		}
	}
	
	
	/**
	 * 编辑日报
	 * @param request
	 * @param daily
	 * @return
	 */
	@RequestMapping(value = "/updateDaily", method = RequestMethod.POST)
	@ResponseBody
	public ResultMessage updateDailyByUseridAndIdAndGroupId(HttpServletRequest request,BusDailyEntity daily){
		ResultMessage result = new ResultMessage();
		// daily_date、completed_task 、 uncompleted_task 、 output_result 、today_plan 、need_help_content 、group_id 、 id 8项从前台获取
		//daily.setUser_id(getUserIdBySecurity()); //获取当前登录用户id
		daily.setLast_update_user(getUserIdBySecurity()); //获取当前登录用户id
		//delete_status、status都为1 ，只有日报已经提交且没有删除才能编辑
		daily.setStatus(1);
		daily.setDelete_status(1);
		daily.setLast_update_date(new Date());
		
		int flug = dailyService.updateDailyByUseridAndIdAndGroupId(daily);
		if(1 == flug){
			result.setMessage("日报修改成功");
			result.setResult("1");
			return result;
		}else{
			result.setMessage("日报修改失败");
			result.setResult("2");
			return result;
		}
	}
	
	/**
	 * 日报信息展示
	 * @param request
	 * @param daily_date
	 * @param group_id
	 * @param user_id
	 * @param page_no  分页页数
	 * @param page_size 每页显示数量
	 * @return
	 */
	//测试路径http://127.0.0.1/daily/queryDaily?page_no=2&page_size=5
	@RequestMapping(value = "/queryDaily", method = RequestMethod.GET)
	@ResponseBody
	public Page<BusDailyEntity> queryAllDaily(HttpServletRequest request,BusDailyEntity daily,int page_no,int page_size){ //
		Page<BusDailyEntity> bde = new Page<BusDailyEntity>();
	    bde.setStart((page_no-1)*page_size); //第一页第一条下标从0开始
        bde.setLength(page_size);
		bde.addParam("user_id", daily.getUser_id());
		bde.addParam("daily_date",daily.getDaily_date());
		bde.addParam("group_id",daily.getGroup_id());
		
		//List findProjectIdByUserId = tUserProjectMapper.findProjectIdByUserId(daily.getUser_id());
		//bde.addParam("group_id_list", findProjectIdByUserId);
		//用户不选择项目名称，则不展示数据；用户选择项目组名称后才展示数据
		if(daily.getGroup_id()==null){
			bde.setData(null);
		}else{
			List<BusDailyEntity> data = dailyService.queryAllDaily(bde);
			bde.setData(data);
		}
		return bde;
	}
	
	//用户登陆后去抓取用户Id
	public int getUserIdBySecurity(){
		TUserEntity userEntity =(TUserEntity) SecurityUtils.getSubject().getPrincipal();
		System.out.println("用户id为："+ userEntity.getId());
		return userEntity.getId();
	}
	
	// http://127.0.0.1:8082/loginUser?username=maoxinxin&password=123456
	// http://127.0.0.1:8082/daily/getUserDataAuthority
	/*@RequestMapping(value = "/getUserDataAuthority", method = RequestMethod.GET)
	@ResponseBody
	public void getUserDataAuthority(){
		TUserEntity userEntity =(TUserEntity) SecurityUtils.getSubject().getPrincipal();
		List<TRoleEntity> roleList = userEntity.getRoleEntities();
		for (TRoleEntity tRoleEntity : roleList) { //先遍历所有权限，从中找到数据权限集合
			if(tRoleEntity.getrType().equals("2")){
				TPermissionEntiry Permission = tRoleEntity.getPermissionEntiries().get(0); //找到数据权限集合
				System.out.println("数据权限++++++++++++++++++："+Permission.getId()+", 名称"+Permission.getName()); //获取到某个用户拥有的数据权限Id
				if(Permission.getId().equals("10")){
					
				}
				}
		}
	}*/
	/**
	 * 根据当前登陆用户id来判断是否有权修改别人提交的日报，登陆用户的id和日报列表中某条数据的user_id一致 才能修改，否则不能修改别人的
	 * @param request
	 * @param user_id 展示列表处某条数据中包含的userId
	 * @return
	 */
	@RequestMapping(value="/compareUserIdAndLoginUser")
	@ResponseBody
	public ResultMessage compareUserIdAndLoginUser(HttpServletRequest request,int user_id){
		ResultMessage result = new ResultMessage();
		//获取当前登陆用户的user_id
		int loginUserId = getUserIdBySecurity();
		//System.out.println("loginUserId用户id :"+ loginUserId +", user_id:"+user_id);
		if(loginUserId == 1 || loginUserId == user_id){ //1是超级管理员
			result.setMessage("允许修改");
			result.setResult("1");
			return result;
		}else{
			result.setMessage("不允许修改其他小伙伴的哦 ！");
			result.setResult("2");
			return result;
		}
	}
	
	/**
	 * 当前登录用户不能删除其他人的日周报
	 *  通过对比当前条数据的中user_id和登录用户的id是否一致，若一致则可删除，不一致不能删除
	 * @param request
	 * @param user_id 页面上点击某一条数据时，是此条数据中的参数
	 * @return
	 */
	@RequestMapping(value="/confirmUserDeletePremission")
	@ResponseBody
	public ResultMessage confirmUserDeletePremission(HttpServletRequest request,int user_id){
		ResultMessage result = new ResultMessage();
		//获取当前登陆用户的user_id
		int loginUserId = getUserIdBySecurity();
		if(loginUserId == 1 || loginUserId == user_id){ //1是超级管理员
			result.setMessage("允许删除");
			result.setResult("1");
			return result;
		}else{
			result.setMessage("不允许删除其他小伙伴的哦 ！");
			result.setResult("2");
			return result;
		}
	}
	
}
