package com.hokai.daily.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class BusDailyEntity extends BaseEntity {

	//id
	public Integer id;
	//用户id
	public Integer user_id;
	//日报选择日期
	@JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8") //将时间戳格式转换成年月日格式
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	public Date daily_date; 
	//昨日已完成任务
	public String completed_task;
	//昨日未完成任务
	public String uncompleted_task;
	//输出成果物
	public String output_result;
	//今日计划
	public String today_plan;
	//需要支持人的id
	public Integer need_help_user_id;
	//需要帮助内容
	public String need_help_content;
	//部门Id
	public Integer department_id;
	//项目组Id
	public Integer group_id;
	//状态 (是否提交  1：提交     0：未提交)
	public Integer status;
	//是否删除状态   1：正常     0：删除
	public Integer delete_status;
	
	//项目组名称 ——> 是从t_project表中获取的
	public String group_name;
	//用户名称 ——> 是从t_user表中获取的
	public String user_name;
	
	public String getGroup_name(){
		return group_name;
	}
	public void setGroup_name(String group_name){
		this.group_name = group_name;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	
	public Integer getDelete_status() {
		return delete_status;
	}
	public void setDelete_status(Integer delete_status) {
		this.delete_status = delete_status;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public Date getDaily_date() {
		return daily_date;
	}
	public void setDaily_date(Date daily_date) {
		this.daily_date = daily_date;
	}
	public String getCompleted_task() {
		return completed_task;
	}
	public void setCompleted_task(String completed_task) {
		this.completed_task = completed_task;
	}
	public String getUncompleted_task() {
		return uncompleted_task;
	}
	public void setUncompleted_task(String uncompleted_task) {
		this.uncompleted_task = uncompleted_task;
	}
	public String getOutput_result() {
		return output_result;
	}
	public void setOutput_result(String output_result) {
		this.output_result = output_result;
	}
	public String getToday_plan() {
		return today_plan;
	}
	public void setToday_plan(String today_plan) {
		this.today_plan = today_plan;
	}
	public Integer getNeed_help_user_id() {
		return need_help_user_id;
	}
	public void setNeed_help_user_id(Integer need_help_user_id) {
		this.need_help_user_id = need_help_user_id;
	}
	public String getNeed_help_content() {
		return need_help_content;
	}
	public void setNeed_help_content(String need_help_content) {
		this.need_help_content = need_help_content;
	}
	public Integer getDepartment_id() {
		return department_id;
	}
	public void setDepartment_id(Integer department_id) {
		this.department_id = department_id;
	}
	public Integer getGroup_id() {
		return group_id;
	}
	public void setGroup_id(Integer group_id) {
		this.group_id = group_id;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public BusDailyEntity() {
		super();
	}
	@Override
	public String toString() {
		return "BusDailyEntity [id=" + id + ", user_id=" + user_id + ", daily_date=" + daily_date + ", completed_task="
				+ completed_task + ", uncompleted_task=" + uncompleted_task + ", output_result=" + output_result
				+ ", today_plan=" + today_plan + ", need_help_user_id=" + need_help_user_id + ", need_help_content="
				+ need_help_content + ", department_id=" + department_id + ", group_id=" + group_id + ", status="
				+ status + ", delete_status=" + delete_status + ", group_name=" + group_name + ", user_name="
				+ user_name + "]";
	}
	
}
