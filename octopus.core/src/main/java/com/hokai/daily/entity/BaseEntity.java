package com.hokai.daily.entity;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.hokai.sysManage.bo.TUserEntity;

public class BaseEntity {

	//根据项目信息获取项目下的所有人员
	public List<TUserEntity> projectPersonInformationList;
	//创建人ID
    public Integer create_user_id;  
    //创建日期       
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date create_date;       
	//最后修改时间   
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date last_update_date; 
	//最后修改人ID
	public Integer last_update_user;
	//删除人id
	public Integer delete_user_id;
	//删除时间   
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date delete_date;
    
    
	public List<TUserEntity> getProjectPersonInformationList() {
		return projectPersonInformationList;
	}
	public void setProjectPersonInformationList(List<TUserEntity> projectPersonInformationList) {
		this.projectPersonInformationList = projectPersonInformationList;
	}
	public Integer getCreate_user_id() {
		return create_user_id;
	}
	public void setCreate_user_id(Integer create_user_id) {
		this.create_user_id = create_user_id;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public Date getLast_update_date() {
		return last_update_date;
	}
	public void setLast_update_date(Date last_update_date) {
		this.last_update_date = last_update_date;
	}
	public Integer getLast_update_user() {
		return last_update_user;
	}
	public void setLast_update_user(Integer last_update_user) {
		this.last_update_user = last_update_user;
	}
	public Integer getDelete_user_id() {
		return delete_user_id;
	}
	public void setDelete_user_id(Integer delete_user_id) {
		this.delete_user_id = delete_user_id;
	}
	public Date getDelete_date() {
		return delete_date;
	}
	public void setDelete_date(Date delete_date) {
		this.delete_date = delete_date;
	}
	@Override
	public String toString() {
		return "BaseEntity [create_user_id=" + create_user_id + ", create_date=" + create_date + ", last_update_date="
				+ last_update_date + ", last_update_user=" + last_update_user + ", delete_user_id=" + delete_user_id
				+ ", delete_date=" + delete_date + "]";
	}
	public BaseEntity() {
		super();
	}
	
}
