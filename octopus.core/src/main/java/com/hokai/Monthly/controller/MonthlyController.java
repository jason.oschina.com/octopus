package com.hokai.Monthly.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.hokai.Monthly.entity.BusMonthlyEntity;
import com.hokai.Monthly.service.MonthlyService;
import com.hokai.base.common.ResultMessage;
import com.hokai.utils.Page;

@Controller
@RequestMapping("/monthly")
public class MonthlyController {

	@Autowired
	MonthlyService monthlyService;

	/**
	 * 上传月报
	 * @param file
	 * @param request
	 * @return
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	@RequestMapping(value="/uploadMonthly",method = RequestMethod.POST)
	@ResponseBody
	public ResultMessage addMonthly(@RequestParam(value = "file_upload", required = true) MultipartFile file,HttpServletRequest request) throws IllegalStateException, IOException{ //
		/*MultipartResolver resolver = new CommonsMultipartResolver(request.getSession().getServletContext());
		MultipartHttpServletRequest multipartRequest = resolver.resolveMultipart(request);
		MultipartFile file = multipartRequest.getFile("file");*/

		ResultMessage result = new ResultMessage();
		//设置相对路径
		if(file !=null){
			monthlyService.addMonthly(file);
			result.setMessage("月报上传成功");
			result.setResult("1");
			return result;
		}else{
			result.setMessage("月报上传失败");
			result.setResult("2");
			return result;
		}
	}
	
	/**
	 * 月报删除
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/deleteMonthly",method = RequestMethod.POST)
	@ResponseBody
	public ResultMessage deleteMonthly(HttpServletRequest request,String id){
		ResultMessage result = new ResultMessage();
		BusMonthlyEntity monthly = new BusMonthlyEntity();
		
		if(StringUtils.isNotBlank(id)){  
			Integer id_val = Integer.parseInt(id);
			monthly.setId(id_val);
		}
		monthly.setStatus(0);
		monthly.setDelete_user_id(1); //获取？？？
		monthly.setDelete_status(0);
		monthly.setDelete_date(new Date());
		
		int num = monthlyService.deleteMonthly(monthly);
		if(num == 1){
			result.setMessage("月报删除成功");
			result.setResult("1");
			return result;
		}else{
			result.setMessage("月报删除失败");
			result.setResult("2");
			return result;
		}
	}

	/**
	 * 月报展示列表
	 * @param request
	 * @param monthly
	 * @param page_no    页数
	 * @param page_size  每页展示条数
	 * @return
	 */
	public Page<BusMonthlyEntity> queryAllMonthly(HttpServletRequest request,BusMonthlyEntity monthly,int page_no,int page_size){
		Page<BusMonthlyEntity> page = new Page<BusMonthlyEntity>();
		page.setStart((page_no-1)*page_size);
		page.setLength(page_size);
		return page;
	}
}
