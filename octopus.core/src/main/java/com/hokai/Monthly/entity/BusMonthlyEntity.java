package com.hokai.Monthly.entity;

import com.hokai.daily.entity.BaseEntity;

public class BusMonthlyEntity extends BaseEntity {

	private Integer id;
	//用户id
	private Integer user_id;
	//文件名称
	private String file_name;
	//文件大小
	private String file_size;
	//文件路径
	private String root_path;
	//相对路径
	private String relative_path;
	//部门Id
	private Integer department_id;
	//项目组Id
	private Integer group_id;
	//状态是否提交  1：提交     0：未提交
	private Integer status;
	//是否删除状态   1：正常     0：删除
	private Integer delete_status;
	
	//项目组名称 ——> 是从t_project表中获取的
	public String group_name;
	//用户名称 ——> 是从t_user表中获取的
	public String user_name;
	
	public String getGroup_name(){
		return group_name;
	}
	public void setGroup_name(String group_name){
		this.group_name = group_name;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getFile_size() {
		return file_size;
	}
	public void setFile_size(String file_size) {
		this.file_size = file_size;
	}
	public String getRoot_path() {
		return root_path;
	}
	public void setRoot_path(String root_path) {
		this.root_path = root_path;
	}
	public String getRelative_path() {
		return relative_path;
	}
	public void setRelative_path(String relative_path) {
		this.relative_path = relative_path;
	}
	public Integer getDepartment_id() {
		return department_id;
	}
	public void setDepartment_id(Integer department_id) {
		this.department_id = department_id;
	}
	public Integer getGroup_id() {
		return group_id;
	}
	public void setGroup_id(Integer group_id) {
		this.group_id = group_id;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getDelete_status() {
		return delete_status;
	}
	public void setDelete_status(Integer delete_status) {
		this.delete_status = delete_status;
	}
	@Override
	public String toString() {
		return "BusMonthlyEntity [id=" + id + ", user_id=" + user_id + ", file_name=" + file_name + ", file_size="
				+ file_size + ", root_path=" + root_path + ", relative_path=" + relative_path + ", department_id="
				+ department_id + ", group_id=" + group_id + ", status=" + status + ", delete_status=" + delete_status
				+ "]";
	}
	public BusMonthlyEntity() {
		super();
	}
	
	
}
