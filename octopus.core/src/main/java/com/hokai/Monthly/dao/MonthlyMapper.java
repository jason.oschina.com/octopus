package com.hokai.Monthly.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import com.hokai.Monthly.entity.BusMonthlyEntity;
import com.hokai.utils.Page;
@Mapper
public interface MonthlyMapper {

	//新增日报
	int addMonthly(BusMonthlyEntity monthly);
	
	//删除日报
	int deleteMonthlyById(BusMonthlyEntity monthly);
	
	//月报展示
	List<BusMonthlyEntity> queryAllMonthly(Page<BusMonthlyEntity> page);
}
