package com.hokai.Monthly.service;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import com.hokai.Monthly.dao.MonthlyMapper;
import com.hokai.Monthly.entity.BusMonthlyEntity;
import com.hokai.utils.Page;

@Service
public class MonthlyServiceImpl implements MonthlyService {

	@Autowired
	MonthlyMapper monthlyMapper;

	@Transactional
	@Override
	public void addMonthly(MultipartFile file) throws IllegalStateException, IOException{ //,String realPath
		System.out.println("service---------------------:"+file);
		if(file != null){
			//获取文件的格
			String extention = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
			//对格式进行筛
			if(extention.equalsIgnoreCase("xls") || extention.equalsIgnoreCase("xlsx")) {

				//获取文件名称
				String fileName = file.getOriginalFilename();

				int size = (int) file.getSize();
				System.out.println(fileName + "-->" + size);
				//在路径下创建文件
				String path = "D:/fileUpload" ;
				File dest = new File(path + "/" + fileName);
				if(!dest.getParentFile().exists()){ //判断文件父目录是否存
					dest.getParentFile().mkdir();
				}

				//上传保存路径
				String uploadPath = path + File.separator + fileName;
				file.transferTo(dest);

				//将文件信息与实体绑定
				BusMonthlyEntity monthlyUpload = new BusMonthlyEntity();
				monthlyUpload.setFile_name(fileName); //文件名称
				monthlyUpload.setFile_size(String.valueOf(file.getSize())); //文件大小
				monthlyUpload.setRelative_path(uploadPath);
				monthlyUpload.setGroup_id(1);  //是否需要？？？？
				monthlyUpload.setUser_id(1);
				monthlyUpload.setCreate_user_id(1);
				monthlyUpload.setCreate_date(new Date());
				monthlyUpload.setStatus(1);
				monthlyUpload.setDelete_status(1);
				//保存至数据库
				monthlyMapper.addMonthly(monthlyUpload);
			}
		}
	}


    /**
     * 删除日报
     */
	@Transactional
	@Override
	public int deleteMonthly(BusMonthlyEntity monthly) {
		return monthlyMapper.deleteMonthlyById(monthly);
	}


	/**
	 * 月报展示列表
	 */
	@Override
	public List<BusMonthlyEntity> queryAllMonthly(Page<BusMonthlyEntity> page) {
		
		return monthlyMapper.queryAllMonthly(page);
	}


	
}
