package com.hokai.Monthly.service;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;
import com.hokai.Monthly.entity.BusMonthlyEntity;
import com.hokai.utils.Page;

public interface MonthlyService {

	//新增日报
	void addMonthly(MultipartFile file) throws IllegalStateException, IOException; //,String realPath

	//删除日报
	int deleteMonthly(BusMonthlyEntity monthly);
	
	List<BusMonthlyEntity> queryAllMonthly(Page<BusMonthlyEntity> page);
}
