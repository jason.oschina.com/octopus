package com.hokai.weekly.controller;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hokai.base.common.ResultMessage;
import com.hokai.sysManage.bo.TUserEntity;
import com.hokai.utils.DateUtil;
import com.hokai.utils.Page;
import com.hokai.weekly.entity.BusWeeklyEntity;
import com.hokai.weekly.service.WeeklyService;

@Controller
@RequestMapping("/weekly")
public class WeeklyController {

	@Autowired
	WeeklyService weeklyService;
	
	/**
	 * 新增周报
	 * @param request
	 * @param weekly
	 * @return
	 */
	@RequestMapping(value="/addWeekly",method = RequestMethod.POST)
	@ResponseBody
	public ResultMessage addWeekly(HttpServletRequest request,BusWeeklyEntity weekly,String weekly_date){//@RequestBody 
		ResultMessage result = new ResultMessage();
		weekly.setUser_id(getUserIdBySecurity()); // 获取当前用户id
		weekly.setCreate_user_id(getUserIdBySecurity()); 
		weekly.setWeekly_date(DateUtil.strToYYMMDDDate(weekly_date));
		weekly.setCreate_date(new Date());
		weekly.setStatus(1);
		weekly.setDelete_status(1);
		int flug = weeklyService.addWeekly(weekly);
		if(flug == 1){
			result.setMessage("周报提交成功");
			result.setResult("1");
			return result;
		}else{
			result.setMessage("周报已经提交,只可编辑/删除");
			result.setResult("2");
			return result;
		}
	}
	/**
	 * 删除周报
	 * @param request
	 * @param weekly
	 * @return
	 */
	@RequestMapping(value="/deleteWeekly")
	@ResponseBody
	public ResultMessage deleteWeeklyByUserIdAndIdAndGroupId(HttpServletRequest request,BusWeeklyEntity weekly){
		ResultMessage result = new ResultMessage();
		//id 、group_id 可从前台数据中返回
		//weekly.setUser_id(getUserIdBySecurity());// ---------------从哪抓取？？？？？？？？
		weekly.setStatus(0);
		weekly.setDelete_status(0);
		weekly.setDelete_date(new Date());
		weekly.setDelete_user_id(getUserIdBySecurity()); // ---------------从哪抓取？？？？？？？？
		int flug = weeklyService.deleteWeeklyByUserIdAndIdAndGroupId(weekly);
		if(flug == 1){
			result.setMessage("周报删除成功");
			result.setResult("1");
			return result;
		}else{
			result.setMessage("周报删除删除");
			result.setResult("2");
			return result;
		}
	}
	
	/**
	 * 编辑周报
	 * @param request
	 * @param weekly
	 * @return
	 */
	@RequestMapping(value="/updateWeekly",method = RequestMethod.POST)
	@ResponseBody
	public ResultMessage updateWeeklyByUseridAndIdAndGroupId(HttpServletRequest request,BusWeeklyEntity weekly){
		ResultMessage result = new ResultMessage();
		
		//id 、weekly_date、 summary_of_last_week、 this_week_plan、 group_id 从前台获取
		//weekly.setUser_id(getUserIdBySecurity()); // 获取当前登录用户id
		weekly.setLast_update_user(getUserIdBySecurity());
		//delete_status、status都为1 ，只有日志已经提交且没有删除才能编辑
		weekly.setStatus(1);
		weekly.setDelete_status(1);
		weekly.setLast_update_date(new Date());
		int flug = weeklyService.updateWeeklyByUseridAndIdAndGroupId(weekly);
		if(flug == 1){
			result.setMessage("周报修改成功");
			result.setResult("1");
			return result;
		}else{
			result.setMessage("周报删除删除");
			result.setResult("2");
			return result;
		}
	}
	
	/**
	 * 周报展示
	 * @param request
	 * @param weekly_date
	 * @param group_id
	 * @param user_id
	 * @param page_no    分页查询第几页(页数)
	 * @param page_size  分页查询每页显示数据条数(每页条数)
	 * @return
	 */
	@RequestMapping(value="/queryWeekly",method = RequestMethod.GET)
	@ResponseBody
	public Page<BusWeeklyEntity> queryAllWeekly(HttpServletRequest request,String weekly_date,String group_id,String user_id,int page_no,int page_size){
		Page<BusWeeklyEntity> page = new Page<BusWeeklyEntity>();
		page.setLength(page_size);
		page.setStart((page_no-1)*page_size);
		page.addParam("weekly_date", weekly_date);
		page.addParam("group_id", group_id);
		page.addParam("user_id", user_id);
		//System.out.println("---------user_id:"+user_id+","+group_id+","+page_size);
		// 先选择项目组后才展示数据，否则不展示
		if(group_id ==null || group_id == ""){
			page.setData(null);
		}else{
			
			List<BusWeeklyEntity> data = weeklyService.queryAllWeekly(page);
			page.setData(data);
		}
		return page;
	}
	
	//用户登陆后去抓取用户Id
	public int getUserIdBySecurity(){
		TUserEntity userEntity =(TUserEntity) SecurityUtils.getSubject().getPrincipal();
		System.out.println("用户id为："+ userEntity.getId());
		return userEntity.getId();
	}
}
