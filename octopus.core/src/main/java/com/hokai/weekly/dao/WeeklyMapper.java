package com.hokai.weekly.dao;
/**
 * 日报
 */
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import com.hokai.utils.Page;
import com.hokai.weekly.entity.BusWeeklyEntity;
@Mapper
public interface WeeklyMapper {

	    //新增周报
		int addWeekly(BusWeeklyEntity weekly);
		
		//删除周报(并非真正删除而是改变数据的状态)
		int deleteWeeklyByUserIdAndIdAndGroupId(BusWeeklyEntity weekly);
		
		//编辑周报
		int updateWeeklyByUseridAndIdAndGroupId(BusWeeklyEntity weekly);
		
		//查询展示周报(包括根据条件查询)
		List<BusWeeklyEntity> queryAllWeekly(Page<BusWeeklyEntity> page);// 
		
		//根据用户id和创建时间查询
		BusWeeklyEntity findWeeklyByUserIdAndDailyDateAndGroupId(Map<String, Object> data);
}
