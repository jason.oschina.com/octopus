package com.hokai.weekly.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hokai.daily.entity.BaseEntity;

public class BusWeeklyEntity extends BaseEntity {

	private Integer id;
	//用户id
	private Integer user_id;
	//填写周报选择日期
	@JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8") //将时间戳格式转换成年月日格式
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date weekly_date;
	//上周总结
	private String summary_of_last_week;
	//本周计划
	private String this_week_plan;
	//部门Id
	private Integer department_id;
	//项目组Id
	private Integer group_id;
	//状态 (是否提交  1：提交     0：未提交)
	public Integer status;
	//是否删除状态   1：正常     0：删除
	public Integer delete_status;
	
	//项目组名称 ——> 是从t_project表中获取的
	public String group_name;
	//用户名称 ——> 是从t_user表中获取的
	public String user_name;
	
	public String getGroup_name(){
		return group_name;
	}
	public void setGroup_name(String group_name){
		this.group_name = group_name;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public Date getWeekly_date() {
		return weekly_date;
	}
	public void setWeekly_date(Date weekly_date) {
		this.weekly_date = weekly_date;
	}
	public String getSummary_of_last_week() {
		return summary_of_last_week;
	}
	public void setSummary_of_last_week(String summary_of_last_week) {
		this.summary_of_last_week = summary_of_last_week;
	}
	public String getThis_week_plan() {
		return this_week_plan;
	}
	public void setThis_week_plan(String this_week_plan) {
		this.this_week_plan = this_week_plan;
	}
	public Integer getDepartment_id() {
		return department_id;
	}
	public void setDepartment_id(Integer department_id) {
		this.department_id = department_id;
	}
	public Integer getGroup_id() {
		return group_id;
	}
	public void setGroup_id(Integer group_id) {
		this.group_id = group_id;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getDelete_status() {
		return delete_status;
	}
	public void setDelete_status(Integer delete_status) {
		this.delete_status = delete_status;
	}
	@Override
	public String toString() {
		return "BusWeeklyEntity [id=" + id + ", user_id=" + user_id + ", weekly_date=" + weekly_date
				+ ", summary_of_last_week=" + summary_of_last_week + ", this_week_plan=" + this_week_plan
				+ ", department_id=" + department_id + ", group_id=" + group_id + ", status=" + status
				+ ", delete_status=" + delete_status + "]";
	}
	public BusWeeklyEntity() {
		super();
	}
    
}
