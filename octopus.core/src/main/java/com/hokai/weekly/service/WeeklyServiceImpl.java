package com.hokai.weekly.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hokai.utils.Page;
import com.hokai.weekly.dao.WeeklyMapper;
import com.hokai.weekly.entity.BusWeeklyEntity;
@Service
public class WeeklyServiceImpl implements WeeklyService {
 
	@Autowired
	WeeklyMapper weeklyMapper;
	
	/**
	 * 新增周报
	 */
	@Override
	public int addWeekly(BusWeeklyEntity weekly) {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("user_id",weekly.getUser_id());
		data.put("weekly_date",weekly.getWeekly_date()); //--------------时间格式的转换可能存在问题
		data.put("group_id",weekly.getGroup_id());
		data.put("status",weekly.getStatus());
		data.put("delete_status",weekly.getDelete_status());
		BusWeeklyEntity weeklyData = weeklyMapper.findWeeklyByUserIdAndDailyDateAndGroupId(data);
		System.out.println("............................: "+ weeklyData);
		//如果当天还没有添加日报，则允许添加
    	if(weeklyData == null){
    		int num = weeklyMapper.addWeekly(weekly);
    		return num; //1 表时提交成功
    	}else{
    		return 2; // 2 代表异常，已经提交了不能在进行提交
    	}
	}

	/**
	 * 删除周报
	 */
	@Override
	public int deleteWeeklyByUserIdAndIdAndGroupId(BusWeeklyEntity weekly) {
		
		return weeklyMapper.deleteWeeklyByUserIdAndIdAndGroupId(weekly);
	}

	/**
	 * 编辑周报
	 */
	@Override
	public int updateWeeklyByUseridAndIdAndGroupId(BusWeeklyEntity weekly) {
		return weeklyMapper.updateWeeklyByUseridAndIdAndGroupId(weekly);
	}

	
	/**
	 * 查询周报展示
	 */
	@Override
	public List<BusWeeklyEntity> queryAllWeekly(Page<BusWeeklyEntity> page) {
		
		return weeklyMapper.queryAllWeekly(page);
	}

}
