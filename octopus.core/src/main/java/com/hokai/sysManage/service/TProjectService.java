package com.hokai.sysManage.service;

import java.util.List;
import java.util.Map;

import com.hokai.sysManage.bo.TProjectEntity;
import com.hokai.sysManage.bo.TUserEntity;
import com.hokai.utils.Page;

public interface TProjectService {
	
	List<TProjectEntity> queryByPage(Page<TProjectEntity> page);

    int deleteByPrimaryKey(Integer id);

    int insert(TProjectEntity record);

    int insertSelective(TProjectEntity record);

    TProjectEntity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TProjectEntity record);
    
    int updateDelFlagByKey(TProjectEntity record);
    
    //为项目分配人员
    int allotUsers(Map<String, Object> paraMap);
    
    //查看所有的工作人员
    List<TUserEntity> queryAllotedUsers(Page<TUserEntity> page);
    
    //查看某项目组下已分配的人员
    List<Integer> queryAllotedUserIds(Page<TUserEntity> page);

}
