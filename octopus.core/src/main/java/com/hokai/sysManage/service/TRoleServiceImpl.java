package com.hokai.sysManage.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hokai.sysManage.bo.TPermissionEntiry;
import com.hokai.sysManage.bo.TRoleEntity;
import com.hokai.sysManage.dao.TPermissionMapper;
import com.hokai.sysManage.dao.TRoleMapper;
import com.hokai.sysManage.dao.TRolePermissionMapper;
import com.hokai.sysManage.dao.TUserRoleMapper;
import com.hokai.utils.Page;

@Service
public class TRoleServiceImpl implements TRoleService{
	
	@Autowired
	private TRoleMapper tRoleMapper;
	
	@Autowired
	private TRolePermissionMapper tRolePermissionMapper;
	
	@Autowired
	private TPermissionMapper tPermissionMapper;
	
	@Autowired
	private TUserRoleMapper  tUserRoleMapper;

	@Override
	public List<TRoleEntity> queryByPage(Page<TRoleEntity> page) {
		// TODO Auto-generated method stub
		return tRoleMapper.queryByPage(page);
	}

	@Transactional
	@Override
	public int deleteByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return tRoleMapper.deleteByPrimaryKey(id);
	}

	@Transactional
	@Override
	public int insert(TRoleEntity record) {
		// TODO Auto-generated method stub
		int num =0;
		Page<TRoleEntity> page=new Page<>();
		page.addParam("name", record.getrName());
		List<TRoleEntity> roleEntities = tRoleMapper.queryByPage(page);
		if(roleEntities==null || ( roleEntities!=null && roleEntities.size()>0)) {
			System.out.println("角色名称重复");
			num = -2;//代表名称重复
			return num;
		}
		record.setrDelFlag(0);
		record.setrCreateTime(new Date());
		record.setrStatus(0);
		return tRoleMapper.insert(record);
	}

	@Transactional
	@Override
	public int insertSelective(TRoleEntity record) {
		// TODO Auto-generated method stub
		return tRoleMapper.insertSelective(record);
	}

	@Override
	public TRoleEntity selectByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return tRoleMapper.selectByPrimaryKey(id);
	}

	@Transactional
	@Override
	public int updateByPrimaryKeySelective(TRoleEntity record) {
		// TODO Auto-generated method stub
		TRoleEntity roleEntity = tRoleMapper.selectByPrimaryKey(record.getId());
		if(roleEntity.getIsBuiltIn()==1) {
			return -10;
		}
		int num =0;
		Page<TRoleEntity> page=new Page<>();
		page.addParam("name", record.getrName());
		page.addParam("noId", record.getId());
		List<TRoleEntity> queryByPage = tRoleMapper.queryByPage(page);
		if(queryByPage==null || ( queryByPage!=null && queryByPage.size()>0)) {
			System.out.println("角色名称重复");
			num = -2;//代表名称重复
			return num;
		}
		record.setrModifyTime(new Date());
		num = tRoleMapper.updateByPrimaryKeySelective(record);
		return num;
	}

	@Transactional
	@Override
	public int updateDelFlagByKey(TRoleEntity record) {
		// TODO Auto-generated method stub
		TRoleEntity roleEntity = tRoleMapper.selectByPrimaryKey(record.getId());
		if(roleEntity.getIsBuiltIn()==1) {
			return -10;
		}
		//根据角色ID删除 用户角色关系
		Map<String, Object> paraMap=new HashMap<>();
		paraMap.put("roleId", record.getId());
		tRolePermissionMapper.deleteByRoleId(paraMap);
		
		tUserRoleMapper.deleteByRoleId(paraMap);
		
		return tRoleMapper.updateByPrimaryKeySelective(record);
	}

	@Transactional
	@Override
	public int allotPermission(Map<String, Object> paraMap) {
		// TODO Auto-generated method stub
		TRoleEntity roleEntity = tRoleMapper.selectByPrimaryKey(Integer.parseInt((String)paraMap.get("roleId")));
		if(roleEntity.getIsBuiltIn()==1) {
			return -10;
		}
		int num =0;
		//首先删除之前的权限
		num = tRolePermissionMapper.deleteByRoleId(paraMap);
		//再添加
		num = tRolePermissionMapper.inserts(paraMap);
		return num;
	}

	@Override
	public List<TPermissionEntiry> queryAllotPermissions(Page<TPermissionEntiry> page) {
		// TODO Auto-generated method stub
		return tPermissionMapper.queryAllotPermissions(page);
	}

}
