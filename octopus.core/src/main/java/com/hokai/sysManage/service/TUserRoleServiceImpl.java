package com.hokai.sysManage.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hokai.sysManage.dao.TUserRoleMapper;

@Service
public class TUserRoleServiceImpl implements TUserRoleService{
	
	@Autowired
	private TUserRoleMapper tUserRoleMapper;

	@Override
	public int inserts(Map<String, Object> paraMap) {
		// TODO Auto-generated method stub
		return tUserRoleMapper.inserts(paraMap);
	}

}
