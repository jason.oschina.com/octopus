package com.hokai.sysManage.service;

import java.util.List;
import java.util.Map;

import com.hokai.sysManage.bo.TRoleEntity;
import com.hokai.sysManage.bo.TUserEntity;
import com.hokai.utils.Page;

public interface TUserService {
	
	List<TUserEntity> queryByPage(Page<TUserEntity> page);

    int deleteByPrimaryKey(Integer id);

    int insert(TUserEntity record);

    int insertSelective(TUserEntity record);

    TUserEntity selectByPrimaryKey(Integer id);
    
    TUserEntity selectByUserName(Page<TUserEntity> page);

    int updateByPrimaryKeySelective(TUserEntity record);
    
    int updateDelFlagByKey(TUserEntity record);
    
    int allotRoles(Map<String, Object> paraMap);
    
    //查看已分配和未分配的角色
    List<TRoleEntity> queryIsAllotRoles(Page<TRoleEntity> page);
    
    int updatePasswordByPrimaryKey(TUserEntity record);

}
