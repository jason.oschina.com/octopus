package com.hokai.sysManage.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hokai.sysManage.bo.TPermissionEntiry;
import com.hokai.sysManage.dao.TPermissionMapper;
import com.hokai.utils.Page;

@Service
public class TPermissionServiceImpl implements TPermissionService{
	
	@Autowired
	private TPermissionMapper tPermissionMapper;

	@Override
	public List<TPermissionEntiry> queryByPage(Page<TPermissionEntiry> page) {
		// TODO Auto-generated method stub
		return tPermissionMapper.queryByPage(page);
	}

	@Transactional
	@Override
	public int deleteByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return tPermissionMapper.deleteByPrimaryKey(id);
	}

	@Transactional
	@Override
	public int insert(TPermissionEntiry record) {
		// TODO Auto-generated method stub
		return tPermissionMapper.insert(record);
	}

	@Transactional
	@Override
	public int insertSelective(TPermissionEntiry record) {
		// TODO Auto-generated method stub
		return tPermissionMapper.insert(record);
	}

	@Override
	public TPermissionEntiry selectByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return tPermissionMapper.selectByPrimaryKey(id);
	}

	@Transactional
	@Override
	public int updateByPrimaryKeySelective(TPermissionEntiry record) {
		// TODO Auto-generated method stub
		return tPermissionMapper.updateByPrimaryKeySelective(record);
	}

}
