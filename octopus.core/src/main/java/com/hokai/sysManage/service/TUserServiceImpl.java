package com.hokai.sysManage.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hokai.sysManage.bo.TRoleEntity;
import com.hokai.sysManage.bo.TUserEntity;
import com.hokai.sysManage.dao.TRoleMapper;
import com.hokai.sysManage.dao.TUserMapper;
import com.hokai.sysManage.dao.TUserProjectMapper;
import com.hokai.sysManage.dao.TUserRoleMapper;
import com.hokai.utils.Page;

@Service
public class TUserServiceImpl implements TUserService {

	@Autowired
	private TUserMapper tUserMapper;

	@Autowired
	private TUserRoleMapper tUserRoleMapper;

	@Autowired
	private TRoleMapper tRoleMapper;
	
	@Autowired
	private TUserProjectMapper tUserProjectMapper;

	@Override
	public List<TUserEntity> queryByPage(Page<TUserEntity> page) {
		// TODO Auto-generated method stub
		return tUserMapper.queryByPage(page);
	}

	@Transactional
	@Override
	public int deleteByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return tUserMapper.deleteByPrimaryKey(id);
	}

	@Transactional
	@Override
	public int insert(TUserEntity record) {
		// TODO Auto-generated method stub
		int num = 0;
		Page<TUserEntity> page = new Page<>();
		page.addParam("name", record.getUsername());
		List<TUserEntity> entities = tUserMapper.queryByPage(page);
		if (entities == null || (entities != null && entities.size() > 0)) {
			System.out.println("用户名称重复");
			num = -2;// 代表名称重复
			return num;
		}
		//判断有无密码
		if(null != record.getPassword() && !record.getPassword().equals("")) {
			SimpleHash simpleHash = new SimpleHash("md5", record.getPassword(), "123");
			String hex = simpleHash.toHex();
			record.setPassword(hex);
		}else {
			record.setPassword("1e191d851b3b49a248f4ea62f6b06410");//123456
		}
		
		record.setStatus(0);
		record.setDelFlag(0);
		record.setCreateTime(new Date());
		return tUserMapper.insert(record);
	}

	@Override
	public int insertSelective(TUserEntity record) {
		// TODO Auto-generated method stub
		return tUserMapper.insertSelective(record);
	}

	@Override
	public TUserEntity selectByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return tUserMapper.selectByPrimaryKey(id);
	}

	@Transactional
	@Override
	public int updateByPrimaryKeySelective(TUserEntity record) {
		// TODO Auto-generated method stub
		int num = 0;
		if(null != record.getPassword() && record.getPassword().equals("")) {
			record.setPassword(null);
		}
		if(null != record.getPassword()) {
			SimpleHash simpleHash = new SimpleHash("md5", record.getPassword(), "123");
			String hex = simpleHash.toHex();
			record.setPassword(hex);
		}
		record.setModifyTime(new Date());
		num = tUserMapper.updateByPrimaryKeySelective(record);
		return num;
	}

	@Override
	public TUserEntity selectByUserName(Page<TUserEntity> page) {
		// TODO Auto-generated method stub
		return tUserMapper.selectByUserName(page);
	}

	@Transactional
	@Override
	public int updateDelFlagByKey(TUserEntity record) {
		// TODO Auto-generated method stub
		// 判断是还是admin账户
		TUserEntity userEntity = tUserMapper.selectByPrimaryKey(record.getId());
		if (userEntity.getUsername().equals("admin")) {
			return -10;
		}
		record.setDeleteTime(new Date());
		//根据人员ID,删除人员和项目关系
		Map<String, Object> paraMap =new HashMap<>();
		paraMap.put("userId", record.getId());
		tUserProjectMapper.deleteByUserId(paraMap);
		
		//根据人员ID,删除人员角色关系
		tUserRoleMapper.deleteByUserId(paraMap);
		return tUserMapper.updateByPrimaryKeySelective(record);
	}

	@Transactional
	@Override
	public int allotRoles(Map<String, Object> paraMap) {
		// TODO Auto-generated method stub
		// 删除现有的
		tUserRoleMapper.deleteByUserId(paraMap);
		// 再添加

		return tUserRoleMapper.inserts(paraMap);
	}

	@Override
	public List<TRoleEntity> queryIsAllotRoles(Page<TRoleEntity> page) {
		// TODO Auto-generated method stub
		return tRoleMapper.queryIsAllotRoles(page);
	}

	@Transactional
	@Override
	public int updatePasswordByPrimaryKey(TUserEntity record) {
		// TODO Auto-generated method stub
		// 将用户输入的密码进行加密与数据库中的进行比较
		SimpleHash simpleHash = new SimpleHash("md5", record.getPassword(), "123");
		String hex = simpleHash.toHex();
		record.setPassword(hex);
		record.setModifyTime(new Date());
		return tUserMapper.updateByPrimaryKeySelective(record);
	}

}
