package com.hokai.sysManage.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hokai.sysManage.bo.TProjectEntity;
import com.hokai.sysManage.bo.TUserEntity;
import com.hokai.sysManage.dao.TProjectMapper;
import com.hokai.sysManage.dao.TUserMapper;
import com.hokai.sysManage.dao.TUserProjectMapper;
import com.hokai.utils.Page;

@Service
public class TProjectServiceImpl implements TProjectService{

	@Autowired
	private TProjectMapper tProjectMapper;
	
	@Autowired
	private TUserMapper tUserMapper;
	
	@Autowired
	private TUserProjectMapper tUserProjectMapper;
	
	@Override
	public List<TProjectEntity> queryByPage(Page<TProjectEntity> page) {
		// TODO Auto-generated method stub
		return tProjectMapper.queryByPage(page);
	}

	@Transactional
	@Override
	public int deleteByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return tProjectMapper.deleteByPrimaryKey(id);
	}

	@Transactional
	@Override
	public int insert(TProjectEntity record) {
		// TODO Auto-generated method stub
		int num = 0;
		//判断项目名称或项目编码有无重复
		Page<TProjectEntity> page=new Page<>();
		page.addParam("name", record.getpName());
		page.addParam("code", record.getpCode());
		List<TProjectEntity> queryByPage = tProjectMapper.queryByPage(page);
		if(queryByPage==null || ( queryByPage!=null && queryByPage.size()>0)) {
			System.out.println("项目名称或编码重复");
			num = -2;//代表名称重复
			return num;
		}
		TUserEntity userEntity =(TUserEntity) SecurityUtils.getSubject().getPrincipal();
		record.setpCreateUserId(userEntity.getId());
		record.setpDelFlag(0);
		record.setpCreateTime(new Date());
		num = tProjectMapper.insert(record);
		return num;
	}

	@Transactional
	@Override
	public int insertSelective(TProjectEntity record) {
		// TODO Auto-generated method stub
		return tProjectMapper.insertSelective(record);
	}

	@Override
	public TProjectEntity selectByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return tProjectMapper.selectByPrimaryKey(id);
	}

	@Transactional
	@Override
	public int updateByPrimaryKeySelective(TProjectEntity record) {
		// TODO Auto-generated method stub
		int num =0;
		Page<TProjectEntity> page=new Page<>();
		page.addParam("name", record.getpName());
		page.addParam("notId", record.getId());
		List<TProjectEntity> queryByPage = tProjectMapper.queryByPage(page);
		if(queryByPage==null || ( queryByPage!=null && queryByPage.size()>0)) {
			System.out.println("项目名称重复");
			num = -2;//代表名称重复
			return num;
		}
		record.setpModifyTime(new Date());
		num = tProjectMapper.updateByPrimaryKeySelective(record);
		return num;
	}

	@Transactional
	@Override
	public int updateDelFlagByKey(TProjectEntity record) {
		// TODO Auto-generated method stub
		
		//根据项目ID,删除人员与项目的关系
		Map<String, Object> paraMap =new HashMap<>();
		paraMap.put("projectId", record.getId());
		tUserProjectMapper.deleteByProjectId(paraMap);
		
		return tProjectMapper.updateByPrimaryKeySelective(record);
	}

	@Transactional
	@Override
	public int allotUsers(Map<String, Object> paraMap) {
		// TODO Auto-generated method stub
		//先删除
		int num =tUserProjectMapper.deleteByProjectId(paraMap);
		//再添加
		String[] userIds = (String[]) paraMap.get("userIds");
		if(userIds!=null && userIds.length>0) {
			num = tUserProjectMapper.inserts(paraMap);
		}
		return num;
	}

	@Override
	public List<TUserEntity> queryAllotedUsers(Page<TUserEntity> page) {
		// TODO Auto-generated method stub
		return tUserMapper.queryAllotedUsers(page);
	}

	@Override
	public List<Integer> queryAllotedUserIds(Page<TUserEntity> page) {
		// TODO Auto-generated method stub
		return tUserMapper.queryAllotedUserIds(page);
	}

}
