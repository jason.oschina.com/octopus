package com.hokai.sysManage.service;

import java.util.List;

import com.hokai.sysManage.bo.TPermissionEntiry;
import com.hokai.utils.Page;

public interface TPermissionService {
	
	List<TPermissionEntiry> queryByPage(Page<TPermissionEntiry> page);

    int deleteByPrimaryKey(Integer id);

    int insert(TPermissionEntiry record);

    int insertSelective(TPermissionEntiry record);

    TPermissionEntiry selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TPermissionEntiry record);


}
