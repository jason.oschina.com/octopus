package com.hokai.sysManage.service;

import java.util.List;
import java.util.Map;

import com.hokai.sysManage.bo.TPermissionEntiry;
import com.hokai.sysManage.bo.TRoleEntity;
import com.hokai.utils.Page;

public interface TRoleService {
	
	List<TRoleEntity> queryByPage(Page<TRoleEntity> page);

    int deleteByPrimaryKey(Integer id);

    int insert(TRoleEntity record);

    int insertSelective(TRoleEntity record);

    TRoleEntity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TRoleEntity record);
    
    int updateDelFlagByKey(TRoleEntity record);
    
    //为某一个用户分配权限
    int allotPermission(Map<String, Object> paraMap);
    
    List<TPermissionEntiry> queryAllotPermissions(Page<TPermissionEntiry> page);

}
