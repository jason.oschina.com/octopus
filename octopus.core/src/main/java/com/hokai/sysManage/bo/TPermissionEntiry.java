package com.hokai.sysManage.bo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/*
* @ClassName: TPermissionEntiry  
* @Description: TODO(权限信息)  
* @author LinYing  
* @date 2018年9月14日  
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class TPermissionEntiry {

	private Integer id;//权限ID
	private Integer type;//权限类型 1:菜单权限 2:数据权限 3:操作权限
	private String name;//权限名称
	private Integer no;//编号
	private Integer pno;//父编号
	private String url;//链接地址
	private String className;//保留字段
	private Integer deleteFlag;//删除标记 0:正常 1:禁用
	private String index;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getNo() {
		return no;
	}
	public void setNo(Integer no) {
		this.no = no;
	}
	public Integer getPno() {
		return pno;
	}
	public void setPno(Integer pno) {
		this.pno = pno;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public Integer getDeleteFlag() {
		return deleteFlag;
	}
	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag = deleteFlag;
	}
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
	
	
	
}
