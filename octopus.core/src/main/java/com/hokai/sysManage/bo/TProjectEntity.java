package com.hokai.sysManage.bo;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/*
* @ClassName: ProjectEntity  
* @Description: TODO(项目组信息表)  
* @author LinYing  
* @date 2018年9月13日  
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TProjectEntity {

	private Integer id;// 项目ID
	private String pCode;// 项目编码
	private String pName;// 项目名称
	private Integer pDelFlag;// 是否删除 0:正常 1:删除
	private Integer pStatus;// 状态 0:正常 1:禁用
	private String pNode;// 备注
	private Integer pCreateUserId;// 创建人ID
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date pCreateTime;// 创建时间
	private Integer pModifyUserId;// 修改人ID
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date pModifyTime;// 修改时间
	private Integer pDeleteUserId;// 删除人ID
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date pDeleteTime;// 删除时间
	private List<TUserEntity> userEntities;//项目下所有的人员
	private List<Integer> userIds;//项目下工作人员 的ID

	
	public void setUserEntities(List<TUserEntity> userEntities) {
		this.userEntities = userEntities;
	}

	public List<Integer> getUserIds() {
		return userIds;
	}

	public void setUserIds(List<Integer> userIds) {
		this.userIds = userIds;
	}

	public List<TUserEntity> getUserEntities() {
		return userEntities;
	}

	public void userIds(List<TUserEntity> userEntities) {
		this.userEntities = userEntities;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getpCode() {
		return pCode;
	}

	public void setpCode(String pCode) {
		this.pCode = pCode;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public Integer getpDelFlag() {
		return pDelFlag;
	}

	public void setpDelFlag(Integer pDelFlag) {
		this.pDelFlag = pDelFlag;
	}

	public Integer getpStatus() {
		return pStatus;
	}

	public void setpStatus(Integer pStatus) {
		this.pStatus = pStatus;
	}

	public String getpNode() {
		return pNode;
	}

	public void setpNode(String pNode) {
		this.pNode = pNode;
	}

	public Integer getpCreateUserId() {
		return pCreateUserId;
	}

	public void setpCreateUserId(Integer pCreateUserId) {
		this.pCreateUserId = pCreateUserId;
	}

	public Date getpCreateTime() {
		return pCreateTime;
	}

	public void setpCreateTime(Date pCreateTime) {
		this.pCreateTime = pCreateTime;
	}

	public Integer getpModifyUserId() {
		return pModifyUserId;
	}

	public void setpModifyUserId(Integer pModifyUserId) {
		this.pModifyUserId = pModifyUserId;
	}

	public Date getpModifyTime() {
		return pModifyTime;
	}

	public void setpModifyTime(Date pModifyTime) {
		this.pModifyTime = pModifyTime;
	}

	public Integer getpDeleteUserId() {
		return pDeleteUserId;
	}

	public void setpDeleteUserId(Integer pDeleteUserId) {
		this.pDeleteUserId = pDeleteUserId;
	}

	public Date getpDeleteTime() {
		return pDeleteTime;
	}

	public void setpDeleteTime(Date pDeleteTime) {
		this.pDeleteTime = pDeleteTime;
	}

}
