package com.hokai.sysManage.bo;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class TRoleEntity {

	private Integer id;//角色ID
	private String rName;//角色名称
	private String rType;//角色分类 1:菜单角色 2:数据角色 3:操作角色
	private Integer rStatus;//角色状态 0:正常 1:禁用
	private String rNode;//备注
	private Integer rDelFlag;//删除状态 0:正常 1:删除
	private Integer rCreateUserId;//创建人ID
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date rCreateTime;//创建时间
	private Integer rModifyUserId;//修改人ID
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date rModifyTime;//修改时间
	private Integer rDeleteUserId;//删除人ID
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date rDeleteTime;//删除时间
	private Integer isBuiltIn;//是否内置  1是  0不是 (内置数据无法删除,修改)
	
	private String rTypeName;
	private String rStatusName;
	private List<TPermissionEntiry> permissionEntiries;


	public Integer getIsBuiltIn() {
		return isBuiltIn;
	}

	public void setIsBuiltIn(Integer isBuiltIn) {
		this.isBuiltIn = isBuiltIn;
	}

	public String getrTypeName() {
		return rTypeName;
	}

	public void setrTypeName(String rTypeName) {
		this.rTypeName = rTypeName;
	}

	public String getrStatusName() {
		return rStatusName;
	}

	public void setrStatusName(String rStatusName) {
		this.rStatusName = rStatusName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getrName() {
		return rName;
	}

	public void setrName(String rName) {
		this.rName = rName;
	}

	/*public Integer getrType() {
		return rType;
	}

	public void setrType(Integer rType) {
		this.rType = rType;
	}*/

	
	public Integer getrStatus() {
		return rStatus;
	}

	public String getrType() {
		return rType;
	}

	public void setrType(String rType) {
		this.rType = rType;
	}

	public void setrStatus(Integer rStatus) {
		this.rStatus = rStatus;
	}

	public String getrNode() {
		return rNode;
	}

	public void setrNode(String rNode) {
		this.rNode = rNode;
	}

	public Integer getrDelFlag() {
		return rDelFlag;
	}

	public void setrDelFlag(Integer rDelFlag) {
		this.rDelFlag = rDelFlag;
	}

	public Integer getrCreateUserId() {
		return rCreateUserId;
	}

	public void setrCreateUserId(Integer rCreateUserId) {
		this.rCreateUserId = rCreateUserId;
	}

	public Date getrCreateTime() {
		return rCreateTime;
	}

	public void setrCreateTime(Date rCreateTime) {
		this.rCreateTime = rCreateTime;
	}

	public Integer getrModifyUserId() {
		return rModifyUserId;
	}

	public void setrModifyUserId(Integer rModifyUserId) {
		this.rModifyUserId = rModifyUserId;
	}

	public Date getrModifyTime() {
		return rModifyTime;
	}

	public void setrModifyTime(Date rModifyTime) {
		this.rModifyTime = rModifyTime;
	}

	public Integer getrDeleteUserId() {
		return rDeleteUserId;
	}

	public void setrDeleteUserId(Integer rDeleteUserId) {
		this.rDeleteUserId = rDeleteUserId;
	}

	public Date getrDeleteTime() {
		return rDeleteTime;
	}

	public void setrDeleteTime(Date rDeleteTime) {
		this.rDeleteTime = rDeleteTime;
	}

	public List<TPermissionEntiry> getPermissionEntiries() {
		return permissionEntiries;
	}

	public void setPermissionEntiries(List<TPermissionEntiry> permissionEntiries) {
		this.permissionEntiries = permissionEntiries;
	}
	
	
	
	
}
