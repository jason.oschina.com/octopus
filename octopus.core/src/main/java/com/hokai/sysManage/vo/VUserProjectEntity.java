package com.hokai.sysManage.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.hokai.sysManage.bo.TUserEntity;

/*
* @ClassName: VUserProjectEntity  
* @Description: TODO()  
* @author LinYing  
* @date 2018年9月17日  
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class VUserProjectEntity extends TUserEntity{
	
	private String projectNames;//用户所在的项目组

	public String getProjectNames() {
		return projectNames;
	}

	public void setProjectNames(String projectNames) {
		this.projectNames = projectNames;
	}
	
	

}
