package com.hokai.sysManage.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.hokai.sysManage.bo.TPermissionEntiry;

@JsonIgnoreProperties(ignoreUnknown=true)
public class VMenusPermission extends TPermissionEntiry{
	
	private List<TPermissionEntiry> childrenPermissions;

	public List<TPermissionEntiry> getChildrenPermissions() {
		return childrenPermissions;
	}

	public void setChildrenPermissions(List<TPermissionEntiry> childrenPermissions) {
		this.childrenPermissions = childrenPermissions;
	}

	
}
