package com.hokai.sysManage.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hokai.base.common.ResultMessage;
import com.hokai.sysManage.bo.TProjectEntity;
import com.hokai.sysManage.bo.TUserEntity;
import com.hokai.sysManage.service.TProjectService;
import com.hokai.utils.Page;

/*
* @ClassName: TRoleController  
* @Description: TODO(系统管理-项目信息Controller层)  
* @author LinYing  
* @date 2018年9月14日  
 */
@Controller
@RequestMapping("/project")
public class TProjectController {

	@Autowired
	private TProjectService tProjectService;

	/**
	 * @Description: TODO(查询项目信息)
	 * @param start:起始查询位置
	 * @param length:每页查询条数
	 * @param name:项目名称
	 * @param code:项目编码
	 * @return Page<TProjectEntity> :返回页面信息和数据信息
	 */
	@RequestMapping("/queryByPage")
	@ResponseBody
	public Page<TProjectEntity> queryByPage(int page_no, int page_size, String projectName, String projectCode) {
		Page<TProjectEntity> page = new Page<>();
		page.setStart((page_no - 1) * page_size);
		page.setLength(page_size);
		page.addParam("projectName", projectName);
		page.addParam("projectCode", projectCode);
		List<TProjectEntity> projectEntities = tProjectService.queryByPage(page);
		page.setData(projectEntities);
		return page;
	}

	
	/**  
	* @Title: insert  
	* @Description: TODO(添加一条项目信息)  
	* @param @param tProjectEntity:项目信息对象,包括项目名称,项目编码,备注
	* @return ResultMessage:返回响应信息
	*/  
	@RequestMapping(value = ("/insert"), method = RequestMethod.POST)
	@ResponseBody
	public ResultMessage insert(TProjectEntity tProjectEntity) {
		ResultMessage resultMessage = new ResultMessage();
		Page<TProjectEntity> page = new Page<>();
		page.setLength(1);
		int num = tProjectService.insert(tProjectEntity);
		if (num == -2) {
			resultMessage.setResult("-2");
			resultMessage.setMessage("项目名称或编码重复,请重新填写!");
		} else if (num == 1) {
			resultMessage.setResult("1");
			resultMessage.setMessage("项目添加成功");
		} else {
			resultMessage.setResult("2");
			resultMessage.setMessage("项目添加失败");
		}
		return resultMessage;
	}

	
	/**  
	* @Title: update  
	* @Description: TODO(根据项目ID修改项目名称和备注)  
	* @param 
	* @return
	*/  
	@RequestMapping(value = ("/update"), method = RequestMethod.POST)
	@ResponseBody
	public ResultMessage update(TProjectEntity record) {
		ResultMessage resultMessage = new ResultMessage();
		Page<TProjectEntity> page = new Page<>();
		page.setLength(1);
		int num = tProjectService.updateByPrimaryKeySelective(record);
		if (num == -2) {
			resultMessage.setResult("-2");
			resultMessage.setMessage("项目名称或编码重复,请重新填写!");
		} else if (num == 1) {
			resultMessage.setResult("1");
			resultMessage.setMessage("项目修改成功");
		} else {
			resultMessage.setResult("2");
			resultMessage.setMessage("项目修改失败");
		}
		return resultMessage;
	}

//	@RequiresPermissions("删除")
	@RequestMapping(value = ("/delete"))
	@ResponseBody
	public ResultMessage delete(TProjectEntity record) {
		ResultMessage resultMessage = new ResultMessage();
		TProjectEntity projectEntity = new TProjectEntity();
		projectEntity.setId(record.getId());
		projectEntity.setpDelFlag(1);
		int num = tProjectService.updateDelFlagByKey(projectEntity);
		if (num == 1) {
			resultMessage.setResult("1");
			resultMessage.setMessage("删除成功!");
		} else {
			resultMessage.setResult("2");
			resultMessage.setMessage("删除失败!");
		}
		return resultMessage;
	}

	/**  
	* @Title: allotUsers  
	* @Description: TODO(为项目分配人员)  
	* @param id:项目ID
	* @param userIds:人员Id
	* @return ResultMessage    返回类型  
	* @throws  
	*/  
	@RequestMapping(value = ("/allotUsers"))
	@ResponseBody
	public ResultMessage allotUsers(String id, @RequestParam(value = "userIds") String[] userIds) {
		ResultMessage resultMessage = new ResultMessage();
		Map<String, Object> paraMap = new HashMap<String, Object>();
		paraMap.put("projectId", id);
		paraMap.put("userIds", userIds);
		int num = tProjectService.allotUsers(paraMap);
		if (num > 0) {
			resultMessage.setResult("1");
			resultMessage.setMessage("分配成功!");
		}else {
			resultMessage.setResult("2");
			resultMessage.setMessage("分配失败!");
		}
		return resultMessage;
	}

	//  type=1:没有参加到项目当中的人员 type =2 代表已经在项目组中的人员

	
	/**  
	* @Description: TODO(查看某项目下所有人员)  
	* @param @param id
	* @return TProjectEntity 返回类型  
	*/  
	@RequestMapping(value = ("/isAllotUsers"), method = RequestMethod.POST)
	@ResponseBody
	public TProjectEntity isAllotUsers(String id) {
		TProjectEntity projectEntity =new TProjectEntity();
		
		Page<TUserEntity> page = new Page<>();
		page.addParam("projectId", id);
		System.out.println("projectId:" + id);
		List<TUserEntity> userEntities = tProjectService.queryAllotedUsers(page);
		List<Integer> userIds = tProjectService.queryAllotedUserIds(page);
		projectEntity.setUserEntities(userEntities);
		projectEntity.setUserIds(userIds);
		return projectEntity;
	}

}
