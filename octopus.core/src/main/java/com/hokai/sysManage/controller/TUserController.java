package com.hokai.sysManage.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hokai.base.common.ResultMessage;
import com.hokai.sysManage.bo.TRoleEntity;
import com.hokai.sysManage.bo.TUserEntity;
import com.hokai.sysManage.service.TUserService;
import com.hokai.utils.Page;

/*
* @ClassName: TRoleController  
* @Description: TODO(系统管理-用户信息Controller层)  
* @author LinYing  
* @date 2018年9月14日  
 */
@Controller
@RequestMapping("/user")
public class TUserController {

	@Autowired
	private TUserService tUserService;

	/**
	 * @Description: TODO(分页查询用户信息)
	 * @param start:查询的起始位置
	 * @param length:每页查询的条数
	 * @param name:用户姓名
	 * @param username:用户登录名
	 * @return Page<TUserEntity> 返回类型
	 */
	@RequestMapping("/queryByPage")
	@ResponseBody
	public Page<TUserEntity> queryByPage(int page_no, int page_size, String name, String loginName) {
		Page<TUserEntity> page = new Page<>();
		page.setStart((page_no - 1) * page_size);
		page.setLength(page_size);
		page.addParam("name", name);
		page.addParam("loginName", loginName);
		List<TUserEntity> userEntities = tUserService.queryByPage(page);
		page.setData(userEntities);
		return page;
	}

	
	
	/**  
	* @Title: insert  
	* @Description: TODO(新增一个用户)  
	* @param record:
	* @return ResultMessage: 返回新增结果信息
	* @throws  
	*/  
	@RequestMapping(value = ("/insert"), method = RequestMethod.POST)
	@ResponseBody
	public ResultMessage insert(TUserEntity record) {
		ResultMessage resultMessage = new ResultMessage();
		Page<TUserEntity> page = new Page<>();
		page.setLength(1);
		int num = tUserService.insert(record);
		if (num == -2) {
			resultMessage.setResult("-2");
			resultMessage.setMessage("用户名重复,请重新填写!");
		} else if (num == 1) {
			resultMessage.setResult("1");
			resultMessage.setMessage("添加用户成功");
		} else {
			resultMessage.setResult("2");
			resultMessage.setMessage("添加用户失败");
		}
		return resultMessage;
	}

	@RequestMapping(value = ("/update"), method = RequestMethod.POST)
	@ResponseBody
	public ResultMessage update(TUserEntity record) {
		ResultMessage resultMessage = new ResultMessage();
		Page<TUserEntity> page = new Page<>();
		page.setLength(1);
		int num = tUserService.updateByPrimaryKeySelective(record);
		if (num == 1) {
			resultMessage.setResult("1");
			resultMessage.setMessage("修改用户成功");
		} else {
			resultMessage.setResult("2");
			resultMessage.setMessage("修改用户失败");
		}
		return resultMessage;
	}

	@RequestMapping(value = ("/delete"), method = RequestMethod.POST)
	@ResponseBody
	public ResultMessage delete(TUserEntity record) {
		ResultMessage resultMessage = new ResultMessage();
		TUserEntity entity = new TUserEntity();
		entity.setId(record.getId());
		entity.setDelFlag(1);
		int num = tUserService.updateDelFlagByKey(entity);
		if (num == 1) {
			resultMessage.setResult("1");
			resultMessage.setMessage("删除成功!");
		}else if(num==-1) {
			resultMessage.setResult("-10");
			resultMessage.setMessage("删除失败,内置数据不能删除!");
		} else {
			resultMessage.setResult("2");
			resultMessage.setMessage("删除失败!");
		}
		return resultMessage;
	}

	// 为用户分配角色
	@RequestMapping(value = ("/allotRoles"), method = RequestMethod.POST)
	@ResponseBody
	public ResultMessage allotRoles(@RequestParam(value = "roleIds") List<Integer> roleIds, String id) {
		ResultMessage resultMessage = new ResultMessage();
		Map<String, Object> paraMap = new HashMap<String, Object>();
		paraMap.put("userId", id);
		paraMap.put("roleIds", roleIds);
		int num = tUserService.allotRoles(paraMap);
		if (num > 0) {
			resultMessage.setResult("1");
			resultMessage.setMessage("分配成功!");
		} else {
			resultMessage.setResult("2");
			resultMessage.setMessage("分配失败!");
		}
		return resultMessage;
	}

	// 查看已分配和未分配的角色
	// 查看某项目下所有人员 type=1:没有参加到项目当中的人员 type =2 代表已经在项目组中的人员
	@RequestMapping(value = ("/isAllotRoles"), method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> isAllotRoles(String id) {
		Map<String, Object> map =new HashMap<>();
		Page<TRoleEntity> page1 = new Page<>();
		List<TRoleEntity> allRoles = tUserService.queryIsAllotRoles(page1);
		Page<TRoleEntity> page2 = new Page<>();
		page2.addParam("userId", id);
		List<TRoleEntity> hasRoles = tUserService.queryIsAllotRoles(page2);
		map.put("allRoles", allRoles);
		map.put("hasRoles", hasRoles);
		return map;
	}
	
	/**  
	* @Title: updateSecurity  
	* @Description: TODO(修改密码)  
	* @param record:传入ID和新密码
	* @return ResultMessage    返回类型  http://localhost:8082/loginUser?username=lilinying&password=234567
	*/  
	@RequestMapping("/updatePassword")
	@ResponseBody
	public ResultMessage updateSecurity(TUserEntity record) {
		ResultMessage resultMessage = new ResultMessage();
		if(record.getPassword()==null || record.getPassword().equals("")) {
			resultMessage.setResult("密码不能为空!");
			resultMessage.setMessage("修改失败!");
			return resultMessage;
		}
		int num = tUserService.updatePasswordByPrimaryKey(record);
		if (num > 0) {
			resultMessage.setResult("1");
			resultMessage.setMessage("修改成功!");
		} else {
			resultMessage.setResult("2");
			resultMessage.setMessage("修改失败!");
		}
		return resultMessage;
	}
	
}
