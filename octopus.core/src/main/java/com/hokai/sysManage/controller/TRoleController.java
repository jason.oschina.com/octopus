package com.hokai.sysManage.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hokai.base.common.ResultMessage;
import com.hokai.sysManage.bo.TPermissionEntiry;
import com.hokai.sysManage.bo.TRoleEntity;
import com.hokai.sysManage.service.TRoleService;
import com.hokai.utils.Page;

/*
* @ClassName: TRoleController  
* @Description: TODO(系统管理-角色信息Controller层)  
* @author LinYing  
* @date 2018年9月14日  
 */
@Controller
@RequestMapping("/role")
public class TRoleController {

	@Autowired
	private TRoleService tRoleService;

	/**
	 * @Description: TODO(查询角色信息)
	 * @param @param
	 *            name:名称名称
	 * @param @param
	 *            type:角色类型
	 * @return Page<TRoleEntity>:页面信息以及角色信息
	 */
	@RequestMapping("/queryByPage")
	@ResponseBody
	public Page<TRoleEntity> queryByPage(int page_no, int page_size, String name, String type) {
		Page<TRoleEntity> page = new Page<>();
		page.setStart((page_no - 1) * page_size);
		page.setLength(page_size);
		page.addParam("name", name);
		page.addParam("type", type);
		List<TRoleEntity> userEntities = tRoleService.queryByPage(page);
		page.setData(userEntities);
		return page;
	}

	@RequestMapping(value = ("/insert"), method = RequestMethod.POST)
	@ResponseBody
	public ResultMessage insert(TRoleEntity record) {
		ResultMessage resultMessage = new ResultMessage();
		Page<TRoleEntity> page = new Page<>();
		page.setLength(1);
		int num = tRoleService.insert(record);
		if (num == -2) {
			resultMessage.setResult("-2");
			resultMessage.setMessage("角色名称重复,请重新填写!");
		} else if (num == 1) {
			resultMessage.setResult("1");
			resultMessage.setMessage("添加角色成功");
		} else {
			resultMessage.setResult("2");
			resultMessage.setMessage("添加角色失败");
		}
		return resultMessage;
	}

	@RequestMapping(value = ("/update"), method = RequestMethod.POST)
	@ResponseBody
	public ResultMessage update(TRoleEntity record) {
		ResultMessage resultMessage = new ResultMessage();
		Page<TRoleEntity> page = new Page<>();
		page.setLength(1);
		int num = tRoleService.updateByPrimaryKeySelective(record);
		if (num == -2) {
			resultMessage.setResult("-2");
			resultMessage.setMessage("角色名称重复,请重新填写!");
		}else if (num == -10) {
			resultMessage.setResult("-10");
			resultMessage.setMessage("修改失败,内置数据不能修改!");
		} else if (num == 1) {
			resultMessage.setResult("1");
			resultMessage.setMessage("修改角色成功");
		} else {
			resultMessage.setResult("2");
			resultMessage.setMessage("修改角色失败");
		}
		return resultMessage;
	}

	@RequestMapping(value = ("/delete"), method = RequestMethod.POST)
	@ResponseBody
	public ResultMessage delete(TRoleEntity record) {
		ResultMessage resultMessage = new ResultMessage();
		TRoleEntity entity = new TRoleEntity();
		entity.setId(record.getId());
		entity.setrDelFlag(1);
		int num = tRoleService.updateDelFlagByKey(entity);
		if (num == 1) {
			resultMessage.setResult("1");
			resultMessage.setMessage("删除成功!");
		} else if(num == -10){
			resultMessage.setResult("-10");
			resultMessage.setMessage("删除失败,内置数据不能删除!");
		}else {
			resultMessage.setResult("2");
			resultMessage.setMessage("删除失败!");
		}
		return resultMessage;
	}

	// 分配权限
	@RequestMapping(value = ("/allotPermission"), method = RequestMethod.POST)
	@ResponseBody
	public ResultMessage allotPermission(String id,@RequestParam(value = "permissionIds") List<String> permissionIds) {
		ResultMessage resultMessage = new ResultMessage();
		Map<String, Object> paraMap = new HashMap<String, Object>();
		paraMap.put("roleId", id);
		paraMap.put("permissionIds", permissionIds);
		System.err.println("roleId:"+id);
		for (int i=0;i<permissionIds.size();i++) {
			System.out.println("permissionId:"+permissionIds.get(i));
		}
		int num = tRoleService.allotPermission(paraMap);
		if (num > 0) {
			resultMessage.setResult("1");
			resultMessage.setMessage("分配成功!");
		} else if(num == -10){
			resultMessage.setResult("-10");
			resultMessage.setMessage("分配失败,内置数据不能分配!");
		}else {
			resultMessage.setResult("2");
			resultMessage.setMessage("分配失败!");
		}
		return resultMessage;
	}

	// 查看已分配和未分配的权限
	// 查看某项目下所有人员 type=1:没有参加到项目当中的人员 type =2 代表已经在项目组中的人员
	@RequestMapping(value = ("/isAllotPermissions"), method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> isAllotPermissions(String id,String pType) {
		Page<TPermissionEntiry> page = new Page<>();
		page.addParam("pType", pType);
		
		List<TPermissionEntiry> allPermissions = tRoleService.queryAllotPermissions(page);
		page.addParam("roleId", id);
		List<TPermissionEntiry> hasPermissions = tRoleService.queryAllotPermissions(page);
		Map<String, Object> map =new HashMap<>();
		map.put("allPermissions", allPermissions);
		map.put("hasPermissions", hasPermissions);
		return map;
	}

}
