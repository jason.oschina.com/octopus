package com.hokai.sysManage.dao;

import java.util.List;
import java.util.Map;

public interface TUserProjectMapper {
	
	int inserts(Map<String, Object> paraMap);
	
	int deleteByProjectId(Map<String, Object> paraMap);
	
	int deleteByUserId(Map<String, Object> paraMap);

	//根据用户id查询用户都在哪些项目组中
	List findProjectIdByUserId(int user_id);
}
