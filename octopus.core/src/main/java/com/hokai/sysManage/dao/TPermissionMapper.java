package com.hokai.sysManage.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.hokai.sysManage.bo.TPermissionEntiry;
import com.hokai.utils.Page;

public interface TPermissionMapper {

	List<TPermissionEntiry> queryByPage(Page<TPermissionEntiry> page);

    int deleteByPrimaryKey(Integer id);

    int insert(TPermissionEntiry record);

    int insertSelective(TPermissionEntiry record);

    TPermissionEntiry selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TPermissionEntiry record);

    //查看当前角色类型下所有未分配的权限和已分配的权限
    List<TPermissionEntiry> queryAllotPermissions(Page<TPermissionEntiry> page);
	
}
