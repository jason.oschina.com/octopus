package com.hokai.sysManage.dao;

import java.util.List;
import java.util.Map;

import com.hokai.sysManage.bo.TRoleEntity;
import com.hokai.utils.Page;

public interface TRoleMapper {

	List<TRoleEntity> queryByPage(Page<TRoleEntity> page);

	int deleteByPrimaryKey(Integer id);

	int insert(TRoleEntity record);

	int insertSelective(TRoleEntity record);

	TRoleEntity selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(TRoleEntity record);

	// 查看已分配和未分配的角色
	List<TRoleEntity> queryIsAllotRoles(Page<TRoleEntity> page);

}
