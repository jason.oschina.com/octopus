package com.hokai.sysManage.dao;

import java.util.Map;

public interface TRolePermissionMapper {

	int inserts(Map<String, Object> paraMap);
	
	int deleteByRoleId(Map<String, Object> paraMap);

}
