package com.hokai.sysManage.dao;

import java.util.Map;

public interface TUserRoleMapper {

	int inserts(Map<String, Object> paraMap);
	
	int deleteByUserId(Map<String, Object> paraMap);
	
	int deleteByRoleId(Map<String, Object> paraMap);
	
}
