package com.hokai.sysManage.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.hokai.sysManage.bo.TProjectEntity;
import com.hokai.utils.Page;

public interface TProjectMapper {
	
	List<TProjectEntity> queryByPage(Page<TProjectEntity> page);

    int deleteByPrimaryKey(Integer id);

    int insert(TProjectEntity record);

    int insertSelective(TProjectEntity record);

    TProjectEntity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TProjectEntity record);

}
