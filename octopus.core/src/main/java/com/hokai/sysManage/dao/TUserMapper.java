package com.hokai.sysManage.dao;

import java.util.List;
import java.util.Map;

import com.hokai.sysManage.bo.TUserEntity;
import com.hokai.utils.Page;

public interface TUserMapper {

	List<TUserEntity> queryByPage(Page<TUserEntity> page);

    int deleteByPrimaryKey(Integer id);

    int insert(TUserEntity record);

    int insertSelective(TUserEntity record);

    TUserEntity selectByPrimaryKey(Integer id);
    
    TUserEntity selectByUserName(Page<TUserEntity> page);

    int updateByPrimaryKeySelective(TUserEntity record);
    
    //为用户分配角色
    int allotRoles(Map<String, Object> paraMap);
    
    //查看所有的用户
    List<TUserEntity> queryAllotedUsers(Page<TUserEntity> page);
    
    //查看某项目下的工作人员
    List<Integer> queryAllotedUserIds(Page<TUserEntity> page);
	
}
