package com.hokai.utils;


import java.lang.reflect.Field;
import java.sql.Connection;
import java.util.Properties;

import org.apache.ibatis.executor.statement.BaseStatementHandler;
import org.apache.ibatis.executor.statement.RoutingStatementHandler;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.scripting.defaults.DefaultParameterHandler;

@Intercepts({
		@Signature(type = StatementHandler.class, method = "prepare", args = { Connection.class, Integer.class }) })
public class MybatisBaseDaoAddInterceptor implements Interceptor {

	public Object intercept(Invocation invocation) throws Throwable {


		if (invocation.getTarget() instanceof BaseStatementHandler) {

			RoutingStatementHandler statementHandler = (RoutingStatementHandler) invocation.getTarget();
			StatementHandler delegate = (StatementHandler) ReflectHelper.getFieldValue(statementHandler, "delegate");

			BoundSql boundSql = delegate.getBoundSql();
			Object obj = boundSql.getParameterObject();
			MappedStatement mappedStatement = (MappedStatement) ReflectHelper.getFieldValue(delegate,
					"mappedStatement");

			if (mappedStatement.getId().equals("com.hokai.utils.BaseDao.add")) {

				System.out.println(obj.getClass());

			}

		}
		return invocation.proceed();
	}

	@Override
	public Object plugin(Object target) {
		if (target instanceof DefaultParameterHandler) {

			DefaultParameterHandler delegate = (DefaultParameterHandler) target;

			MappedStatement mappedStatement = (MappedStatement) ReflectHelper.getFieldValue(delegate,
					"mappedStatement");

			String methodId = mappedStatement.getId();
			String className = methodId.substring(0, methodId.lastIndexOf("."));
			String methodName = methodId.substring(methodId.lastIndexOf(".") + 1);

			Class targClass = null;
			try {
				targClass = Class.forName(className);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

			if (targClass != null) {
				
				
				Class[] parentInterfaces = targClass.getInterfaces();
				for (Class pInterface : parentInterfaces) {
					
					if(pInterface == BaseDao.class){
						
						if("add".equals(methodName)) {
							BoundSql boundSql = (BoundSql) ReflectHelper.getFieldValue(delegate, "boundSql");
							Object obj = boundSql.getParameterObject();
							

							Field[] fields = obj.getClass().getDeclaredFields();
							for (Field field : fields) {
								PrimaryKey pk = field.getAnnotation(PrimaryKey.class);
								if (pk != null) {
									String pkName = field.getName();
									String[] args = {pkName};
									ReflectHelper.setFieldValue(mappedStatement, "keyProperties", args);
								}

							}

							
						}
						
					}
					
					
					
					
					
					
				}
				
				
			

				
			}

		}


		if (target instanceof StatementHandler) {
			return Plugin.wrap(target, this);
		} else {

			/*
			 * RoutingStatementHandler statementHandler =
			 * (RoutingStatementHandler)invocation.getTarget(); StatementHandler delegate =
			 * (StatementHandler) ReflectHelper.getFieldValue(statementHandler, "delegate");
			 * 
			 * 
			 * 
			 * BoundSql boundSql = delegate.getBoundSql(); Object obj =
			 * boundSql.getParameterObject(); MappedStatement mappedStatement =
			 * (MappedStatement)ReflectHelper.getFieldValue(delegate, "mappedStatement");
			 * 
			 * if
			 * (mappedStatement.getId().equals("com.yyxt.purm.dao.billManageDao.BaseDao.add"
			 * )) {
			 * 
			 * System.out.println(obj.getClass());
			 * 
			 * }
			 */

			return target;
		}
	}

	@Override
	public void setProperties(Properties properties) {
		// TODO Auto-generated method stub

	}

	/*public static void main(String[] args) throws ClassNotFoundException {

		String abc = "a.b.c";
		System.out.println(abc.substring(0, abc.lastIndexOf(".")));
		System.out.println(abc.substring(abc.lastIndexOf(".") + 1));

		String className = abc.substring(0, abc.lastIndexOf("."));

		Object obj = Class.forName(className);
		if (obj != null && (obj instanceof BaseDao<?>)) {

		}

	}*/

}