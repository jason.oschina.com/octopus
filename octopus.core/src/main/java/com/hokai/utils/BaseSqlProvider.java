package com.hokai.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.jdbc.SQL;

public class BaseSqlProvider<T> {
	
	
	 @Options
	 public String add(T bean) {
		 
		SQL sql = new SQL();
		
		 
		Class clazz = bean.getClass();
		
		
		String tableName = clazz.getSimpleName();
		
		
		sql.INSERT_INTO(tableName);
		
		
		List<Field> fields = getFields(clazz);
		for (Field field : fields) {
			
			field.setAccessible(true);
			
			
		
			
			
			String column = field.getName();
			
			if (column.equals("id")) {
				continue;
			}
			
			System.out.println(column);
		
			sql.VALUES(column, String.format("#{"+column+",jdbcType=VARCHAR}"));
			
		}
		
		
		
		return sql.toString();
	 }
	 
	 
	 
	 public String delete(T bean) {
		 
			SQL sql = new SQL();
			
			 
			Class clazz = bean.getClass();
			
			
			String tableName = clazz.getSimpleName();
			
			
			sql.DELETE_FROM(tableName);
			
			
			
			
			List<Field> primaryKeyField = getPrimarkKeyFields(clazz);
			
			
			if (!primaryKeyField.isEmpty()) {
				
				for (Field pkField : primaryKeyField) {
					pkField.setAccessible(true);
					sql.WHERE(pkField.getName()+"="+ String.format("#{"+pkField.getName()+"}"));
				}
				
			}else {
				
				sql.WHERE(" 1= 2");
				
				throw new RuntimeException("对象中未包含PrimaryKey属性");
			}
			
			
			
			
			
			
			
			
			
			return sql.toString();
			
			
			
			
			
			
			
	}



	private List<Field> getPrimarkKeyFields(Class clazz) {
		
		List<Field> primaryKeyField = new ArrayList<>();
		List<Field> fields = getFields(clazz);
		for (Field field : fields) {
			field.setAccessible(true);
			PrimaryKey key = field.getAnnotation(PrimaryKey.class);
			if (key!= null) {
				primaryKeyField.add(field);
			}
			
		}
		return primaryKeyField;
	}
	
	
	private List<Field> getFields(Class clazz) {
		
		List<Field> fieldList = new ArrayList<>();
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
			Exclude key = field.getAnnotation(Exclude.class);
			if (key== null) {
				fieldList.add(field);
			}
			
		}
		return fieldList;
	}
	 
	 
	public String get(T bean) {
		
		
		
		
		
		
		SQL sql = new SQL();
		
		 
		Class clazz = bean.getClass();
		
		
		String tableName = clazz.getSimpleName();
		
		
		sql.SELECT("*").FROM(tableName);
		
		
		
		
		List<Field> primaryKeyField = getPrimarkKeyFields(clazz);
		
		
		if (!primaryKeyField.isEmpty()) {
			
			for (Field pkField : primaryKeyField) {
				pkField.setAccessible(true);
				sql.WHERE(pkField.getName()+"="+ String.format("#{"+pkField.getName()+"}"));
			}
			
		}else {
			
			sql.WHERE(" 1= 2");
			
			throw new RuntimeException("对象中未包含PrimaryKey属性");
		}
		
		
		
		
		
		
		
		
		
		return sql.toString();
		
	}
	
	
	
	public String update(T bean) {
		
		
		SQL sql = new SQL();
		
		 
		Class clazz = bean.getClass();
		
		
		String tableName = clazz.getSimpleName();
		
		
		sql.UPDATE(tableName);
		
		
		
		List<Field> fields = getFields(clazz);
		for (Field field : fields) {
			
			field.setAccessible(true);
			
			
		
			
			
			String column = field.getName();
			
			if (column.equals("id")) {
				continue;
			}
			
			System.out.println(column);
		
			sql.SET(column + "=" + String.format("#{"+column+",jdbcType=VARCHAR}"));
			
		}
		
		
		
		List<Field> primaryKeyField = getPrimarkKeyFields(clazz);
		
		
		if (!primaryKeyField.isEmpty()) {
			
			for (Field pkField : primaryKeyField) {
				pkField.setAccessible(true);
				sql.WHERE(pkField.getName()+"="+ String.format("#{"+pkField.getName()+"}"));
			}
			
		}else {
			
			sql.WHERE(" 1= 2");
			
			throw new RuntimeException("对象中未包含PrimaryKey属性");
		}
		
		
		
		return sql.toString();
		
		
		
		
		
	}
	
	
	
	
	/*public static void main(String[] args) {
		String sql = new BaseSqlProvider<T_USERS>().delete(new T_USERS());
		System.out.println(sql);
	}*/
	
	
	
	
		 
	 
	 

}
