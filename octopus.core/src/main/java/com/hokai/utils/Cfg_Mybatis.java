package com.hokai.utils;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Cfg_Mybatis {
	
	@Bean
    public MybatisSpringPageInterceptor pageHelper() {  
       MybatisSpringPageInterceptor pageHelper = new MybatisSpringPageInterceptor();  
        pageHelper.setDialect("mysql");
        pageHelper.setPageSqlId(".*query$");
        return pageHelper;  
    }  

}
