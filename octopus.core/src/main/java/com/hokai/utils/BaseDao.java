package com.hokai.utils;

import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

public interface BaseDao<T> {
	
	
	@InsertProvider(method = "add",type=BaseSqlProvider.class)
	@Options(useGeneratedKeys=true, keyProperty="_id")
	public void add(T bean);
	
	
	@DeleteProvider(method = "delete",type=BaseSqlProvider.class)
	public void delete(T bean);
	
	
	@SelectProvider(method = "get",type=BaseSqlProvider.class)
	public T get(T bean);
	
	
	@UpdateProvider(method = "update",type=BaseSqlProvider.class)
	public void update(T bean);

}
