package com.hokai.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Page<T> {

	public static final int DEFAULT_PAGE_SIZE = 10;



	protected List<T> data; // 当前页记录List形式
	
	private Map<String, Object> params = new HashMap<String, Object>();//设置页面传递的查询参数

    private int start = 0;
    
    private int length = -1;
      
//    private int draw = 1;
    
    private int recordsTotal;//总共消息条数
    
    private int recordsFiltered;
    
  
    public Integer getRows() {  
        return length;  
    }  
  
  
    public Integer getPage() {  
        return (this.start+1)/this.length+1;  
    }  
  
  

	
	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}


	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}  
	
	
	public void addParam(String key , Object value){
		this.params.put(key, value);
	}


	public static int getDefaultPageSize() {
		return DEFAULT_PAGE_SIZE;
	}
    
	
	
	
	
	
	
    

}
