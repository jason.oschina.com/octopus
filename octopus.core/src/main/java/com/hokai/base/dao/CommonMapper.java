package com.hokai.base.dao;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import com.hokai.sysManage.bo.TPermissionEntiry;
import com.hokai.sysManage.bo.TProjectEntity;
import com.hokai.sysManage.bo.TUserEntity;

@Mapper
public interface CommonMapper {

	//查询所有用户;   u.id not in(1) 不显示管理员
	List<TUserEntity> queryUserNameByGroupId(int group_id);
	
	//根据用户id查询所在项目组后以下拉选列表展示
	List<TProjectEntity> queryProjectByUserId(Map<String, Object> data);
	
	//根据用户数据权限名称查询数据权限对应的id  10：个人、11：所在项目组、12：全员
	TPermissionEntiry findPermissionIdByPermissionName(String name);
}
