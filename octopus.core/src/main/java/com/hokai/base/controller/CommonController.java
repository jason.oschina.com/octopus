package com.hokai.base.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hokai.base.common.ResultMessage;
import com.hokai.base.dao.CommonMapper;
import com.hokai.daily.entity.BusDailyEntity;
import com.hokai.sysManage.bo.TPermissionEntiry;
import com.hokai.sysManage.bo.TRoleEntity;
import com.hokai.sysManage.bo.TUserEntity;
import com.hokai.sysManage.dao.TUserProjectMapper;
@Controller
@RequestMapping("/base/common")
public class CommonController {


	@Autowired
	CommonMapper commonMapper;
	
	@Autowired
	TUserProjectMapper tUserProjectMapper;

	@Value("${dataPermissionPerson}")
	int dataPermissionPerson; //数据权限 个人

	@Value("${dataPermissionWorkingGroup}")
	int dataPermissionWorkingGroup; //数据权限 所在项目组

	@Value("${dataPermissionWhole}")
	int dataPermissionWhole; // 数据权限 全员
	
	/**
	 * 姓名下拉选：根据所选项目组查询出该项目组下的人员名称
	 * @return 
	 */
	@RequestMapping(value = "/queryUserNameByGroupId", method = RequestMethod.GET)
	@ResponseBody
	public ResultMessage queryUserNameByGroupId(String group_id){
		ResultMessage result = new ResultMessage();
		if(StringUtils.isNotBlank(group_id)){
			Integer group_id_val = Integer.parseInt(group_id);
			result.setMessage("用户名称信息返回成功");
			result.setResult("1");
			result.setResultData(commonMapper.queryUserNameByGroupId(group_id_val));
		}else{
			result.setMessage("用户名称信息返回失败");
			result.setResult("2");
			result.setResultData(null);
		}
		return result;
	}

	@RequestMapping(value = "/queryProjectName", method = RequestMethod.GET)
	@ResponseBody
	public ResultMessage queryProjectByUserId(int flug){
		ResultMessage result = new ResultMessage();
		TUserEntity user = new TUserEntity();
		Map<String, Object> data = new HashMap<String, Object>();
		if(flug == 1){ //获取用户id，在填写日报时选择自己的user_id查询出所在项目组
			System.out.println(((TUserEntity) SecurityUtils.getSubject().getPrincipal()).getId());
			user.setId(((TUserEntity) SecurityUtils.getSubject().getPrincipal()).getId()); // ------------------ 获取用户id
		}else{ //为查询页面上项目组下拉选提供数据，此处不根据user_id关联
			user.setId(null);
		}
		data.put("user_id", user.getId());
		result.setResultData(commonMapper.queryProjectByUserId(data));
		return result;
	}

	/**
	 * 根据用户id和用户所拥有的数据权限查询用户所在的项目组
	 * @return
	 */
	@RequestMapping(value = "/queryProjectNameByUserIdAndDataPermissionId", method = RequestMethod.GET)
	@ResponseBody
	public ResultMessage queryProjectNameByUserIdAndDataPermissionId(){
		ResultMessage result = new ResultMessage();
		TUserEntity user = new TUserEntity();
		Map<String, Object> data = new HashMap<String, Object>();
		//获取用户id
		//System.out.println(((TUserEntity) SecurityUtils.getSubject().getPrincipal()).getId());

		user.setId(((TUserEntity) SecurityUtils.getSubject().getPrincipal()).getId()); // ------------------ 获取用户id
		data.put("user_id", user.getId());
		result.setResultData(commonMapper.queryProjectByUserId(data));
		result.setMessage("项目名称信息返回成功");
		result.setResult("1");
		return result;
	}

	/*public void getUserDataAuthority(int userId){
		BusDailyEntity daily = new BusDailyEntity();
		TUserEntity userEntity =(TUserEntity) SecurityUtils.getSubject().getPrincipal();
		List<TRoleEntity> roleList = userEntity.getRoleEntities();
		for (TRoleEntity tRoleEntity : roleList) { //先遍历所有权限，从中找到数据权限集合
			if(tRoleEntity.getrType().equals("2")){
				TPermissionEntiry Permission = tRoleEntity.getPermissionEntiries().get(0); //找到数据权限集合
				System.out.println("数据权限++++++++++++++++++："+Permission.getId()+", 名称"+Permission.getName()); //获取到某个用户拥有的数据权限Id
				
				if(Permission.getId().equals(dataPermissionPerson)){ //数据权限是个人
					//daily.setProjectPersonInformationList(userId); //将用户的user_id保存到list中
				}else if(Permission.getId().equals(dataPermissionWorkingGroup)){ //数据权限是所在项目组；
                    //1.先要查出用户在哪几个项目组中
					tUserProjectMapper.findProjectIdByUserId(userId);
					//2.查询处这几个项目组下都有哪些人员；在日报展示列表的查询sql中判断用户在不在列用到in
					
				}else{ //数据权限是全员；查出全部项目组
					
				}
				
			}
		}
	}*/
}
