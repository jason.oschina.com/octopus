package com.hokai.base.common;

public class ResultMessage {

	private int code;
	
	private String result; //1.ok 2.error

	private String message;

	private Object resultData;

	public String getResult() {
		return result;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getResultData() {
		return resultData;
	}

	public void setResultData(Object resultData) {
		this.resultData = resultData;
	}


	@Override
	public String toString() {
		return "ResultMessage [code=" + code + ", result=" + result + ", message=" + message + ", resultData="
				+ resultData + "]";
	}

	public ResultMessage() {
		super();
	}
	
}
