package com.hokai.base.config;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;
import org.apache.shiro.crypto.hash.SimpleHash;

/**
 * 
* @ClassName: CredentialsMatcher  
* @Description: TODO(继承SimpleCredentialsMatcher 实现对密码的比较)  
* @author LinYing  
* @date 2018年8月20日  
*
 */
public class CredentialsMatcher extends SimpleCredentialsMatcher{

	@Override
	public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
		// TODO Auto-generated method stub
		UsernamePasswordToken utoken=(UsernamePasswordToken) token;
		//获取用户输入的密码,返回char[]
		String inPass =new String(utoken.getPassword());
		//将用户输入的密码进行加密与数据库中的进行比较
		SimpleHash simpleHash=new SimpleHash("md5", inPass, "123");
		String hex = simpleHash.toHex();
		System.out.println("加密后的密码:"+hex);
		//获取数据库的密码
		String dbPass = (String) info.getCredentials();
		
		/*AesCipherService aesCipherService=new AesCipherService();
		aesCipherService.setKeySize(128);
		Key key = aesCipherService.generateNewKey();
		//加密
		String enPass = aesCipherService.encrypt(inPass.getBytes(), key.getEncoded()).toHex();
		//解密
		String dePass = new String(aesCipherService.decrypt(Hex.decode(enPass), key.getEncoded()).getBytes());
		System.out.println("加密:"+enPass);
		System.out.println("解密:"+dePass);*/
		
		//进行比较
		return this.equals(hex, dbPass);
	}
	
}
