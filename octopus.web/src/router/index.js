import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)



//路由懒加载
export default new Router({
  routes: [
    {
        path: '/',
        name: '登录',
        component: resolve => require(['../components/Login.vue'],resolve) ,
    },  
    {
        path: '/index',
        name: 'HelloWorld',
        //redirect: '/dayWorkList',
        component: resolve => require(['../components/HelloWorld.vue'],resolve),
        children:[
        	/*{
	        	path: '',
		        name: '',
		        component: resolve => require([''],resolve)
        	}*/
            {
                path: '/dayWork',
                name: '日报填写',
                component: resolve => require(['../components/page/dayWork.vue'],resolve)
            },{
                path: '/weekWork',
                name: '周报填写',
                component: resolve => require(['../components/page/weekWork.vue'],resolve)
            },{
                path: '/monthWork',
                name: '月报填写',
                component: resolve => require(['../components/page/monthWork.vue'],resolve)
            },{
                path: '/',
                name: '日报列表',
                component: resolve => require(['../components/page/dayWorkList.vue'],resolve)
            },{
                path: '/weekWorkList',
                name: '周报列表',
                component: resolve => require(['../components/page/weekWorkList.vue'],resolve)
            },{
                path: '/monthWorkList',
                name: '月报列表',
                component: resolve => require(['../components/page/monthWorkList.vue'],resolve)
            },{
                path: '/itemInfo',
                name: '项目信息',
                component: resolve => require(['../components/page/itemInfo.vue'],resolve)
            },{
                path: '/userInfo',
                name: '用户信息',
                component: resolve => require(['../components/page/userInfo.vue'],resolve)
            },{
                path: '/roleInfo',
                name: '角色信息',
                component: resolve => require(['../components/page/roleInfo.vue'],resolve)
            }
        ]
    }
  ]
})

