// import "babel-polyfill";
// import promise from 'es6-promise';
// promise.polyfill();
import axios from 'axios'
import {Message} from 'element-ui'


axios.interceptors.request.use(config => {
  if (config.method === 'post') {
    config.data = config.data
  }
  return config;
}, err => {
  Message.error({
    message: '请求超时!'
  });
  return Promise.resolve(err);
})

axios.interceptors.response.use(res => {
  if (res.status && res.status == 200 && res.data.status == 'error') {
    Message.error({
      message: res.data.msg
    });
    return;
  }
  return res;
}, err => {
  if (err.response.status == 504 || err.response.status == 404) {
    Message.error({
      message: '服务器被吃了⊙﹏⊙∥'
    });
  } else if (err.response.status == 403) {
    Message.error({
      message: '权限不足,请联系管理员!'
    });
  } else if (err.response.status == 886) {
    Message.error({
      message: '帐号或密码错误!'
    });
  } else if (err.response.status == 500) {
    Message.error({
      message:err.response.data.message
    });
  } else {
    Message.error({
      message: '未知错误!'
    });
  }
  return Promise.resolve(err);
})

//let base = '';
let base = window.g.ApiUrl;
//let base = 'http://192.168.1.71:8081/octopus-server'; //打包上线开启 注释掉api

export const postRequest = (url, params) => {
  //var url = "/api" + url;
  return axios({
    method: 'post',
    url: `${base}${url}`,
    data: params,
    transformRequest: [function (data) {
      let ret = ''
      for (let it in data) {
        ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
      }
      return ret
    }],
    headers: {
      // 'Content-Type': 'application/json;charset=UTF-8',
      // 'Access-Control-Allow-Origin': '*',
      // 'Access-Control-Allow-Methods':'PUT,GET,POST,OPTIONS',
      // 'Access-Control-Allow-Headers':'X-Requested-With, Content-Type, X-File-Name'
    }
  });
}

export const uploadFileRequest = (url, params) => {
  //var url = "/api" + url;
  return axios({
    method: 'post',
    url: `${base}${url}`,
    data: params,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods':'PUT,GET,POST,OPTIONS',
      'Access-Control-Allow-Headers':'X-Requested-With, Content-Type, X-File-Name'
    }
  });
}

export const putRequest = (url, params) => {
  //var url = "/api" + url;
  return axios({
    method: 'put',
    url: `${base}${url}`,
    data: params,
    transformRequest: [function (data) {
      let ret = ''
      for (let it in data) {
        ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
      }
      return ret
    }],
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'PUT,GET,POST,OPTIONS',
      'Access-Control-Allow-Headers': 'X-Requested-With, Content-Type, X-File-Name'
    }
  });
}

export const deleteRequest = (url) => {
  //var url = "/api" + url;
  return axios({
    method: 'delete',
    url: `${base}${url}`
  });
}

export const getRequest = (url) => {
  //var url = "/api" + url;
  return axios({
    method: 'get',
    url: `${base}${url}`
  });
}